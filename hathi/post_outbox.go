package hathi

import (
	"database/sql"
	"log"
	"strings"
	"net/http"
	//"gitlab.com/hathi-social/hathi-server/activitypub"
)

// The outbox POST handler calls HandleOutboxActivity to deal with objects
// it recieves. This method deals with distributing the activity to the
// proper handler, storing the Activity in the database, and adding a
// reference to the outbox collection.
// Propagation and side effects are handled by the activity-specific handlers.
func (c *Core)HandleOutboxActivity(tx *sql.Tx, state *OutboxPosting) (err error) {
	state.Activity.Id = c.NewID()
	activityType := state.Activity.GetType()
	state.Activity.Origin = state.ActorID // <-- this might be wrong
	if activityType != "Create" {
		// Create has to do special handling of its sub-object so it isn't
		// included in this sequence
		c.trawlEntity(&state.Activity, state.Subs, false)
		// Create also has to deal with replies in the Create vs object
	}
	// Do side effects and propagations for most Activities.
	// Do all the handling for special Activities like Create.
	switch activityType {
	case "Accept":
		err = c.HandleOutAccept(tx, state)
	case "Add":
	case "Announce":
	case "Arrive":
	case "Block":
	case "Create":
		err = c.HandleOutCreate(tx, state)
	case "Delete":
		err = c.HandleOutDelete(tx, state)
	case "Dislike":
	case "Flag":
	case "Follow":
		err = c.HandleOutFollow(tx, state)
	case "Ignore":
	case "Invite":
	case "Join":
	case "Leave":
	case "Like":
	case "Listen":
	case "Move":
	case "Offer":
	case "Question":
	case "Reject":
		err = c.HandleOutReject(tx, state)
	case "Read":
	case "Remove":
	case "TentativeReject":
	case "TentativeAccept":
		err = c.HandleOutTentativeAccept(tx, state)
	case "Travel":
	case "Undo":
	case "Update":
	case "View":
	}

	// Had an error; this covers errors both internally and in the submission
	if err != nil {
		return err
	}
	// Store the Activity in the database
	c.db.SetObject(tx, &state.Activity)
	if err != nil {
		return err
	}
	activityID := state.Activity.GetID()
	c.db.SetPrivilege(tx, activityID, state.ActorID, "owner", true)
	// Distribute replies
	if state.Activity.InReplyTo != nil {
		for _, replyTarget := range state.Activity.InReplyTo {
			isRemote, err := c.IsIDRemote(replyTarget)
			if err != nil {
				continue // Skip this?
			}
			if isRemote == false {
				c.HandleReply(tx, activityID, replyTarget)
			}
		}
	}
	// Do standard propagation + extras
	state.Prop = append(state.Prop, state.Activity.To...)
	state.Prop = append(state.Prop, state.Activity.Bto...)
	state.Prop = append(state.Prop, state.Activity.Cc...)
	state.Prop = append(state.Prop, state.Activity.Bcc...)
	state.Prop = append(state.Prop, state.Activity.Audience...)
	c.Propagate(tx, &state.Activity, state.Spawned, state.Prop)
	// We handle adding to the outbox, the activities handlers handle other
	// side effects
	c.db.ItemsAppend(tx, state.OutboxID, activityID)
	return nil
}

func (c *Core)PostOutboxActivity(state *OutboxPosting) (statusCode int, externalError string, location string, err error) {
	tx, err := c.db.BeginTransaction()
	defer c.db.EndTransaction(tx, &err)
	actor, err := c.db.GetObject(tx, state.ActorID)
	if err != nil {
		return http.StatusInternalServerError, "", "", err
	}
	if actor == nil {
		if Logging {
			log.Println("Outbox POST error: invalid Actor ID", state.ActorID)
		}
		return http.StatusBadRequest, "Invalid Actor ID", "", nil
	} else if actor.IsActor() != true {
		return http.StatusBadRequest, "Target is not an Actor", "", nil
	}
	state.OutboxID = actor.Outbox
	err = c.HandleOutboxActivity(tx, state)
	if err == nil {
		return http.StatusCreated, "", string(state.Activity.GetID()), nil
	} else {
		if Logging {
			log.Println("Outbox POST handling error;", err)
		}
		errText := err.Error()
		if strings.HasPrefix(errText, "Incorrect field:") {
			return http.StatusBadRequest, err.Error(), "", nil
		} else if strings.HasPrefix(errText, "Not allowed:") {
			return http.StatusUnauthorized, errText, "", nil
		} else {
			return http.StatusBadRequest, "Can't POST object", "", nil
		}
	}
}
