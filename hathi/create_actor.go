package hathi

import (
	//"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
	"log"
	"net/http"
	"database/sql"
)

// This is all temporary until a better Actor creation system can be created.

func (c *Core)HandleCreateActor(tx *sql.Tx, state *OutboxPosting) (err error) {
	state.Activity.Id = c.NewID()
	actor := state.Subs[state.Activity.ActObject]
	c.trawlEntity(actor, state.Subs, false)
	actor.Id = state.ActorID
	err = c.AddActor(tx, actor)
	if err != nil {
		return err
	}
	state.Activity.ActObject = actor.GetID()
	c.trawlEntity(&state.Activity, state.Subs, false)
	err = c.db.SetObject(tx, &state.Activity)
	if err != nil {
		return err
	}
	err = c.db.ItemsAppend(tx, actor.Outbox, state.Activity.GetID())
	if err != nil {
		return err
	}
	return nil
}

func (c *Core)PostActor(state *OutboxPosting) (statusCode int, externalError string, location string, err error) {
	tx, err := c.db.BeginTransaction()
	defer c.db.EndTransaction(tx, &err)
	actor, err := c.db.GetObject(tx, state.ActorID)
	if err != nil {
		return http.StatusInternalServerError, "", "", err
	}
	if actor != nil {
		if Logging {
			log.Println("Attempted duplicate Actor Creation", state.ActorID)
		}
		return http.StatusBadRequest, "Actor already exists", "", nil
	}
	if Logging {
		log.Println("Outbox POST Actor creation")
	}
	err = c.HandleCreateActor(tx, state)
	if err == nil {
		return http.StatusCreated, "", string(state.Activity.GetID()), nil
	} else {
		if Logging {
			log.Println("Outbox POST Actor creation error;", err)
		}
		return http.StatusInternalServerError, "Couldn't create Actor", "", nil
	}
}
