package hathi

import (
	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
	"database/sql"
	"net/http"
	"log"
)

func (c *Core)HandleInboxActivity(tx *sql.Tx, remoteP bool, inboxID activitypub.HathiID, actorID activitypub.HathiID, activity *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	if tx == nil {
		tx, err = c.db.BeginTransaction()
		if err != nil {
			return err
		}
		defer c.db.EndTransaction(tx, &err)
	}
	if remoteP == false {
		// have it locally, only need to make sure this Actor is authorized
	} else {
		// Need to shadow the object
		coreObj := activity.GetCore().(*activitypub.Object)
		coreObj.Shadow = coreObj.GetID()
		coreObj.Id = c.NewID()
	}
	// send to handlers
	activityType := activity.GetType()
	switch activityType {
	case "Accept":
		c.HandleInAccept(tx, remoteP, activity, subs)
	case "Add":
	case "Announce":
	case "Arrive":
	case "Block":
	case "Create":
		c.HandleInCreate(tx, remoteP, activity, subs)
	case "Delete":
	case "Dislike":
	case "Flag":
	case "Follow":
		c.HandleInFollow(tx, remoteP, activity, subs)
	case "Ignore":
	case "Invite":
	case "Join":
	case "Leave":
	case "Like":
	case "Listen":
	case "Move":
	case "Offer":
	case "Question":
	case "Reject":
		c.HandleInReject(tx, remoteP, activity, subs)
	case "Read":
	case "Remove":
	case "TentativeReject":
	case "TentativeAccept":
		c.HandleInTentativeAccept(tx, remoteP, activity, subs)
	case "Travel":
	case "Undo":
	case "Update":
	case "View":
	}
	activityID := activity.GetID()
	if remoteP == true {
		// We wait to store a shadow as it may be changed by the handler
		c.db.SetObject(tx, activity)
		// Distribute replies. Local propogations already handled this.
		if activity.InReplyTo != nil {
			for _, replyTarget := range activity.InReplyTo {
				isRemote, err := c.IsIDRemote(replyTarget)
				if err != nil {
					continue // Skip this?
				}
				if isRemote == false {
					c.HandleReply(tx, activityID, replyTarget)
				}
			}
		}
	}
	c.db.ItemsAppend(tx, inboxID, activityID)
	return nil
}

func (c *Core)PostInbox(remoteP bool, actorID activitypub.HathiID, activity *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (statusCode int, externalError string, err error) {
	tx, err := c.db.BeginTransaction()
	defer c.db.EndTransaction(tx, &err)
	actor, err := c.db.GetObject(tx, actorID)
	if err != nil {
		return http.StatusInternalServerError, "", err
	}
	if actor == nil {
		// No Actor, or any object at all here
		if Logging {
			log.Println("Inbox POST error; no actor, or anything")
		}
		return http.StatusNotFound, "", nil
	}
	if actor.IsActor() == false {
		// Not an Actor
		if Logging {
			log.Println("Inbox POST error; no actor")
		}
		return http.StatusBadRequest, "", nil
	}
	inboxID := actor.Inbox
	err = c.HandleInboxActivity(tx, remoteP, inboxID, actorID, activity, subs)
	if err == nil {
		return http.StatusCreated, "", nil
	} else {
		return http.StatusBadRequest, "", nil
	}
}
