package hathi

import (
	"fmt"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gitlab.com/hathi-social/hathi-server/dbase"
	"os"
	"strings"
	"net/url"
	"net/http"
	"time"
	"io/ioutil"
	"encoding/json"
	"bytes"
	"log"
	"database/sql"
)

const HATHI_MAJOR_VERSION = 0
const HATHI_MINOR_VERSION = 1
const HATHI_BUGFIX_VERSION = 0

var Logging bool

var Engine *Core

type OutboxPosting struct {
	ActorID  activitypub.HathiID
	OutboxID activitypub.HathiID
	Activity activitypub.Object
	Subs     map[activitypub.HathiID]*activitypub.Object
	Prop     []activitypub.HathiID // Propagation
	Spawned  map[activitypub.HathiID]*activitypub.Object
	ReplyTos []activitypub.HathiID
	ReplyID  activitypub.HathiID // Hold the ID of the correct reply Object
}

type propagationRequest struct {
	Obj     *activitypub.Object
	Embeds  map[activitypub.HathiID]*activitypub.Object
	Targets []activitypub.HathiID
}

type targetNode struct {
	Id        activitypub.HathiID
	RetryTime time.Time
	Tries     uint8
}

type queueNode struct {
	Obj     *activitypub.Object // The object we need to push
	Embeds  map[activitypub.HathiID]*activitypub.Object
	Targets []targetNode // The initial propagation targets. Could be anything.
	Inboxes []targetNode // The actual inboxes we need to post to.
	Seen    []activitypub.HathiID
	Encoded []byte // So we don't encode this over and over.
}

type DumpData struct {
	Objects    []*activitypub.Object
	Privileges map[activitypub.HathiID]map[activitypub.HathiID][]string
	DumpInfo struct {
		MajorVersion  uint
		MinorVersion  uint
		BugFixVersion uint
		DumpTime      string
	}
}

// Core is the root object
type Core struct {
	nextID       uint64
	LocalURL     string
	LocalPort    string
	db           *dbase.HathiDB
	// Remote propagation
	objectQueues map[activitypub.HathiID]queueNode
	propRequests chan propagationRequest
}

func InitEngine(url string, port string, dbfile string) (err error) {
	isNew := false
	Engine = new(Core)
	Engine.LocalURL = url
	Engine.LocalPort = port
	if _, err = os.Stat(dbfile); os.IsNotExist(err) {
		isNew = true
	}
	Engine.db, err = dbase.OpenDatabase("sqlite3", dbfile)
	if err != nil {
		return err
	}
	if isNew {
		Engine.db.Initialize()
		if err != nil {
			return err
		}
	}
	Engine.objectQueues = make(map[activitypub.HathiID]queueNode)
	Engine.propRequests = make(chan propagationRequest, 16) // Arbitrary size
	return nil
}

func (c *Core)GetObject(objID activitypub.HathiID) (obj *activitypub.Object, err error) {
	return c.db.GetObject(nil, objID)
}

func (c *Core)NewID() (id activitypub.HathiID) {
	// Generates a numeric ID
	localid := c.nextID
	c.nextID++
	return activitypub.HathiID(fmt.Sprintf("http://%s:%s/%x",
		c.LocalURL, c.LocalPort, localid))
}

func (c *Core)MakeIDFromLocalID(localid string) (id activitypub.HathiID) {
	// Generates a specific ID from a base
	// Used for actor and special object IDs
	if Logging {
		log.Println("Building ID from:", localid)
	}
	for localid[0] == '/' {
		localid = strings.TrimPrefix(localid, "/")
	}
	for localid[len(localid)-1] == '/' {
		localid = strings.TrimSuffix(localid, "/")
	}
	return activitypub.HathiID(fmt.Sprintf("http://%s:%s/%s",
		c.LocalURL, c.LocalPort, localid))
}

func (c *Core)IsIDRemote(id activitypub.HathiID) (remoteP bool, err error) {
	url, err := url.Parse(string(id))
	if err != nil {
		return false, err
	}
	hostMatch :=  url.Hostname() == c.LocalURL
	portMatch := false
	if (url.Port() == "") && (c.LocalPort == "8080") {
		portMatch = true
	} else if url.Port() == c.LocalPort {
		portMatch = true
	}
	if hostMatch && portMatch {
		return false, nil
	} else {
		return true, nil
	}
}

// Placeholder until I figure out how this should be done
func (c *Core)AddActor(tx *sql.Tx, a *activitypub.Object) (err error) {
	if tx == nil {
		tx, err = c.db.BeginTransaction()
		if err != nil {
			return err
		}
		defer c.db.EndTransaction(tx, &err)
	}
	actID := a.GetID()
	// Setup collections
	in := activitypub.Object{Type: "OrderedCollection", Id: actID + "/inbox"}
	a.Inbox = in.GetID()
	out := activitypub.Object{Type: "OrderedCollection", Id: actID + "/outbox"}
	a.Outbox = out.GetID()
	followers := activitypub.Object{Type: "OrderedCollection",
		Id: actID + "/followers"}
	a.Followers = followers.GetID()
	following := activitypub.Object{Type: "OrderedCollection",
		Id: actID + "/following"}
	a.Following = following.GetID()
	// Commit
	err = c.db.SetObject(tx, a)
	if err != nil {
		return err
	}
	err = c.db.SetObject(tx, &in)
	if err != nil {
		return err
	}
	err = c.db.SetObject(tx, &out)
	if err != nil {
		return err
	}
	err = c.db.SetObject(tx, &followers)
	if err != nil {
		return err
	}
	err = c.db.SetObject(tx, &following)
	if err != nil {
		return err
	}
	return nil
}

func (c *Core)HandleReply(tx *sql.Tx, replyID activitypub.HathiID, targetID activitypub.HathiID) (err error) {
	// TODO: remote replies
	// TODO: propogation to inboxes
	// Get the target
	target, err := c.db.GetObject(tx, targetID)
	if err != nil {
		return err
	}
	if target == nil {
		// No target, bail
		return nil
	}
	// Get the replies collection, or make one if it doesn't exist
	if target.Replies == "" { // no reply collection yet, make one
		repliesCol := activitypub.Object{Type: "Collection", Id: c.NewID()}
		err = c.db.SetObject(tx, &repliesCol)
		if err != nil {
			return nil
		}
		target.Replies = repliesCol.GetID()
		err = c.db.SetObject(tx, target)
		if err != nil {
			return err
		}
	}
	// Assign
	err = c.db.ItemsAppend(tx, target.Replies, replyID)
	if err != nil {
		return err
	}
	return nil
}

func (c *Core)Propagate(tx *sql.Tx, a *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object, targetIDs []activitypub.HathiID) {
	// TODO: Add propogation for replies
	targetIDs = preparePropList(targetIDs, a.Origin)
	remoteList := []activitypub.HathiID{}
	for _, id := range targetIDs {
		r, err := c.IsIDRemote(id)
		if err != nil {
			continue
		}
		if r == true {
			remoteList = append(remoteList, id)
		} else {
			actor, _ := c.db.GetObject(tx, id)
			if actor == nil {
				continue
			}
			if !actor.IsActor() && !actor.IsCollection() {
				actor = nil // Not an actor or collection
				// We do need to handle propagation to Collections
				continue
			}
			c.HandleInboxActivity(tx, false, actor.Inbox, id, a, subs)
		}
	}
	if len(remoteList) > 0 {
		c.RequestRemotePropagation(a, subs, remoteList)
	}
}

func (c *Core)RequestRemotePropagation(a *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object, targetIDs []activitypub.HathiID) {
	req := propagationRequest{Obj:a, Embeds:subs, Targets:targetIDs}
	c.propRequests <- req
}

func (c *Core)DoRemotePropagation() {
	for true {
		// Check for prop requests and add
		select {
		case req := <-c.propRequests:
			objID := req.Obj.GetID()
			if _, ok := c.objectQueues[objID]; ok {
				// Already exists...... what to do?
			} else {
				node := queueNode{Obj:req.Obj, Embeds:req.Embeds}
				node.Targets = make([]targetNode, len(req.Targets))
				for i, target := range req.Targets {
					node.Targets[i].Id = target
				}
				c.objectQueues[objID] = node
			}
		default:
		}
		for id, queue := range c.objectQueues {
			if len(queue.Targets) > 0 {
				getObjectsForQueue(&queue)
			}
			if len(queue.Inboxes) > 0 {
				pushInboxesFromQueue(&queue)
			}
			if len(queue.Targets) == 0 && len(queue.Inboxes) == 0 {
				// We don't have anything left to propagate, remove the queue
				delete(c.objectQueues, id)
			}
		}
		// replace with more sophisticated limiter later
		time.Sleep(100 * time.Millisecond)
	}
}

func attemptObjectGet(target targetNode) (newTargets []targetNode, newInboxes []targetNode, seen bool) {
	newTargets = make([]targetNode, 0) // Targets we need to try next time
	newInboxes = make([]targetNode, 0) // Inboxes we have found and can pass on
	resp, err := http.Get(string(target.Id))
	if err != nil {
		// connection failed, should expand the checks later
		t := getNextRetryTime(target.Tries)
		if t.IsZero() { // Too many retries
			return []targetNode{}, []targetNode{}, false
		}
		target.Tries++
		newTargets = append(newTargets, target)
		return []targetNode{target}, newInboxes, false
	}
	defer resp.Body.Close()
	rawData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// data read failed, should expand the checks later
		t := getNextRetryTime(target.Tries)
		if t.IsZero() { // Too many retries
			return []targetNode{}, []targetNode{}, true
		}
		target.Tries++
		newTargets = append(newTargets, target)
		return newTargets, newInboxes, false
	}
	status := resp.StatusCode
	if status == http.StatusNotFound {
		// Target doesn't exist
		return []targetNode{}, []targetNode{}, true
	} else if status != http.StatusOK {
		// Unspecified status code
		// drop for now
		return []targetNode{}, []targetNode{}, true
	}
	
	// Ok, by this point we should have an object
	targetObj, _, err := activitypub.DecodeJSON(rawData)
	if err != nil {
		// JSON error, retry
		t := getNextRetryTime(target.Tries)
		if t.IsZero() { // Too many retries
			return []targetNode{}, []targetNode{}, false
		}
		target.Tries++
		newTargets = append(newTargets, target)
		return newTargets, newInboxes, false
	}
	
	if targetObj.IsActor() {
		// target is an Actor, get its inbox
		newInboxes = append(newInboxes,
			targetNode{targetObj.Inbox, time.Time{}, 0})
		// Handled this Actor, push it to seen
		return newTargets, newInboxes, true
	} else if targetObj.IsCollection() {
		// target is a list of objects, push them to the target list
		for _, id := range targetObj.Items {
			newNode := targetNode{id, time.Time{}, 0}
			newTargets = append(newTargets, newNode)
		}
		// Handled this collection, push it to seen
		return newTargets, newInboxes, true
	}
	return []targetNode{}, []targetNode{}, false
}

func getObjectsForQueue(q *queueNode) {
	currentTime := time.Now()
	newTargets := make([]targetNode, 0) // Targets we need to try next time
	newInboxes := make([]targetNode, 0) // Inboxes we have found and can pass on
	TARGET:
	for _, target := range q.Targets {
		for _, s := range q.Seen { // De-duplicate
			if s == target.Id {
				continue TARGET
			}
		}
		if target.RetryTime.IsZero() || currentTime.After(target.RetryTime) {
			// Expired, time to retry
			tgts, inboxes, seen := attemptObjectGet(target)
			newTargets = append(newTargets, tgts...)
			newInboxes = append(newInboxes, inboxes...)
			if seen {
				q.Seen = append(q.Seen, target.Id)
			}
		}
	}
	q.Targets = newTargets
	q.Inboxes = newInboxes
}

func getNextRetryTime(tries uint8) (t time.Time){
	// 1min, 10min, 30min, 90min (repeat)
	// We get the try count before increment
	var inc time.Duration
	switch tries {
	case 0:
		inc = time.Duration(time.Minute)
	case 1:
		inc = time.Duration(time.Minute * 10)
	case 2:
		inc = time.Duration(time.Minute * 30)
	case 255: // Too many retries, bail
		return time.Time{}
	default:
		inc = time.Duration(time.Minute * 90)
	}
	t = time.Now()
	t = t.Add(inc)
	return t
}

func pushInboxesFromQueue(q *queueNode) {
	// Fill q.Encoded with encoded and embedded object
	if q.Encoded == nil {
		enc := activitypub.MapifyObjectWithEmbeds(q.Obj, q.Embeds)
		q.Encoded, _ = json.Marshal(enc)
	}
	newInboxes := make([]targetNode, 0)
	currentTime := time.Now()
	for _, target := range q.Inboxes {
		if target.RetryTime.IsZero() || currentTime.After(target.RetryTime) {
			req, err := http.NewRequest("POST", string(target.Id),
				bytes.NewBuffer(q.Encoded))
			if err != nil {
				t := getNextRetryTime(target.Tries)
				if t.IsZero() { // Too many retries
					continue
				}
				target.Tries++
				target.RetryTime = t
				newInboxes = append(newInboxes, target)
				continue
			}
			req.Header.Set("Content-Type", "application/json")
			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				t := getNextRetryTime(target.Tries)
				if t.IsZero() { // Too many retries
					continue
				}
				target.Tries++
				target.RetryTime = t
				newInboxes = append(newInboxes, target)
				continue
			}
			defer resp.Body.Close()
			status := resp.StatusCode
			if status == http.StatusCreated {
				// Sucess
				resp.Body.Close()
				continue
			} else {
				// Error
				t := getNextRetryTime(target.Tries)
				if t.IsZero() { // Too many retries
					continue
				}
				target.Tries++
				target.RetryTime = t
				newInboxes = append(newInboxes, target)
				continue
			}
		}
	}
	q.Inboxes = newInboxes
}

func (c *Core)DumpDB() (dump *DumpData, err error) {
	dump = new(DumpData)
	tx, err := c.db.BeginTransaction()
	if err != nil {
		return nil, err
	}
	defer c.db.EndTransaction(tx, &err)
	// Suck out the objects
	objIDs, err := c.db.GetAllObjectIDs(tx)
	if err != nil {
		return nil, err
	}
	dump.Objects = make([]*activitypub.Object, len(objIDs))
	for index, objID := range objIDs {
		obj, err := c.db.GetObject(tx, objID)
		if err != nil {
			return nil, err
		}
		dump.Objects[index] = obj
	}
	// Suck out the privileges
	privIDs, err := c.db.GetAllPrivilegeIDs(tx)
	if err != nil {
		return nil, err
	}
	dump.Privileges = make(map[activitypub.HathiID]map[activitypub.HathiID][]string, 0)
	for _, privID := range privIDs {
		priv, err := c.db.GetObjectPrivileges(tx, privID)
		if err != nil {
			return nil, err
		}
		dump.Privileges[privID] = priv
	}
	// Misc data
	// TODO: change to major, minor, bugfix uints
	dump.DumpInfo.MajorVersion = HATHI_MAJOR_VERSION
	dump.DumpInfo.MinorVersion = HATHI_MINOR_VERSION
	dump.DumpInfo.BugFixVersion = HATHI_BUGFIX_VERSION
	t := time.Now()
	dump.DumpInfo.DumpTime = t.Format(time.RFC3339)
	return dump, nil
}

func (c *Core)LoadDB(dump *DumpData) (err error) {
	tx, err := c.db.BeginTransaction()
	if err != nil {
		return err
	}
	defer c.db.EndTransaction(tx, &err)
	// Inject objects
	for _, obj := range dump.Objects {
		err = c.db.SetObject(tx, obj)
		if err != nil {
			return err
		}
	}
	// Inject privileges
	for privID, privs := range dump.Privileges {
		err = c.db.SetObjectPrivileges(tx, privID, privs)
		if err != nil {
			return err
		}
	}
	return nil
}
