package hathi

import (
	"database/sql"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	"fmt"
	"time"
)

func (c *Core) HandleOutAccept(tx *sql.Tx, state *OutboxPosting) (err error) {
	// Need to know the type of object we are accepting
	acptObj, err := c.db.GetObject(tx, state.Activity.ActObject)
	if err != nil {
		return err
	}
	switch acptObj.GetType() {
	case "Follow":
		// Add follower, we need to get the actor's followers Collection
		followee, err := c.db.GetObject(tx, state.Activity.Origin)
		if err != nil {
			return err
		}
		follower := acptObj.Origin
		// Add the actor who is following to the target's followers
		c.db.ItemsAppend(tx, followee.Followers, follower)
		state.Prop = append(state.Prop, follower)
	}
	return nil
}

func (c *Core) HandleOutAdd(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutAnnounce(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutArrive(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutBlock(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutCreate(tx *sql.Tx, state *OutboxPosting) (err error) {
	actCore := state.Activity.GetCore().(*activitypub.Object)
	obj := state.Subs[state.Activity.ActObject]
	objCore := obj.GetCore().(*activitypub.Object)
	objCore.Id = c.NewID()
	state.Activity.ActObject = obj.GetID()
	// Find and create spawnable sub-objects
	// Replace temp IDs
	c.trawlEntity(&state.Activity, state.Subs, false)
	state.Spawned = c.recursiveSpawnTrawl(obj, state.Subs, 8)
	// Sync propagation addresses
	syncLists(&actCore.To, &objCore.To, true)
	syncLists(&actCore.Bto, &objCore.Bto, true)
	syncLists(&actCore.Cc, &objCore.Cc, true)
	syncLists(&actCore.Bcc, &objCore.Bcc, true)
	syncLists(&actCore.Audience, &objCore.Audience, true)
	// Do reply stuff
	if obj.InReplyTo != nil {
		for _, replyTarget := range obj.InReplyTo {
			c.HandleReply(tx, obj.GetID(), replyTarget)
		}
	}
	// Commit
	c.db.SetPrivilege(tx, obj.GetID(), state.ActorID, "owner", true)
	c.db.SetObject(tx, obj)
	// Store any spawned objects
	for spawnID, spawnObj := range state.Spawned {
		c.db.SetObject(tx, spawnObj)
		c.db.SetPrivilege(tx, spawnID, state.ActorID, "owner", true)
	}
	state.Spawned[obj.GetID()] = obj
	return nil
}

func (c *Core) HandleOutDelete(tx *sql.Tx, state *OutboxPosting) (err error) {
	// Check that this actor is allowed to delete the object
	allowed, err := c.db.CheckPrivilege(tx, state.Activity.ActObject,
		state.Activity.Origin, "owner")
	if err != nil {
		return err
	}
	if allowed == false {
		return fmt.Errorf("Not allowed: Actor %s does not own Object %s",
			state.ActorID, state.Activity.ActObject)
	}
	// Get target object
	oldObj, err := c.db.GetObject(tx, state.Activity.ActObject)
	if err != nil {
		return err
	}
	if oldObj == nil { // Object does not exist
		return nil
	}
	// Get original propagation, we will inform the target object's original
	// targets of the deletion.
	oldCore := oldObj.GetCore().(*activitypub.Object)
	syncLists(&oldCore.To, &state.Activity.To, false)
	syncLists(&oldCore.Bto, &state.Activity.Bto, false)
	syncLists(&oldCore.Cc, &state.Activity.Cc, false)
	syncLists(&oldCore.Bcc, &state.Activity.Bcc, false)
	syncLists(&oldCore.Audience, &state.Activity.Audience, false)
	// Create replacement tombstone
	currentTime := time.Now()
	tomb := activitypub.Object{
		Type:"Tombstone", Id:oldObj.GetID(), FormerType:oldObj.GetType(),
		Deleted:currentTime.Format(time.RFC3339), To:state.Activity.To,
		Bto:state.Activity.Bto, Cc:state.Activity.Cc, Bcc:state.Activity.Bcc,
		Audience:state.Activity.Audience,
	}
	// Replace object with tombstone
	c.db.SetObject(tx, &tomb)
	c.db.SetPrivilege(tx, tomb.GetID(), state.ActorID, "owner", true)
	return nil
}

func (c *Core) HandleOutDislike(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutFlag(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutFollow(tx *sql.Tx, state *OutboxPosting) (err error) {
	//coreObj := act.GetCore().(*activitypub.Object)
	c.trawlEntity(&state.Activity, state.Subs, false)
	state.Prop = append(state.Prop, state.Activity.ActObject)
	// Do side effects
	return nil
}

func (c *Core) HandleOutIgnore(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutInvite(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutJoin(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutLeave(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutLike(tx *sql.Tx, state *OutboxPosting) (err error) {
	coreObj := state.Activity.GetCore().(*activitypub.Object)
	coreObj.Id = c.NewID()
	c.trawlEntity(&state.Activity, state.Subs, false)
	state.Prop = append(state.Prop, state.Activity.ActObject)
	// Add to our "liked" collection
	return nil
}

func (c *Core) HandleOutListen(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutMove(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutOffer(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutQuestion(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutReject(tx *sql.Tx, state *OutboxPosting) (err error) {
	// Need to know the type of object we are rejecting
	acptObj, err := c.db.GetObject(tx, state.Activity.ActObject)
	if err != nil {
		return err
	}
	switch acptObj.GetType() {
	case "Follow":
	}
	return nil
}

func (c *Core) HandleOutRead(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutRemove(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutTentativeReject(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutTentativeAccept(tx *sql.Tx, state *OutboxPosting) (err error) {
	return c.HandleOutAccept(tx, state)
}

func (c *Core) HandleOutTravel(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutUndo(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutUpdate(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}

func (c *Core) HandleOutView(tx *sql.Tx, state *OutboxPosting) (err error) {
	return nil
}
