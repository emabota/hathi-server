package hathi

import (
	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
	"database/sql"
)

func (c *Core)HandleInAccept(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	// Figure out how to handle remotes
	// Need to know the type of object we are accepting
	acptObj, err := c.db.GetObject(tx, act.ActObject)
	if err != nil {
		return err
	}
	switch acptObj.GetType() {
	case "Follow":
		// Add followee, we need to get the actor's following collection
		follower, err := c.db.GetObject(tx, acptObj.Origin)
		if err != nil {
			return err
		}
		followee := act.Origin
		// Add the who is being followed to the follower's following list
		c.db.ItemsAppend(tx, follower.Following, followee)
	}
	return nil
}

func (c *Core)HandleInAdd(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInAnnounce(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInArrive(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInBlock(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInCreate(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	if remoteP == true {
		subObj := subs[act.ActObject]
		subCore := subObj.GetCore().(*activitypub.Object)
		subCore.Id = c.NewID()
		c.db.SetObject(tx, subObj)
		act.ActObject = subObj.GetID()
	}
	return nil
}

func (c *Core)HandleInDelete(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInDislike(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInFlag(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInFollow(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	// Nothing to do here
	return nil
}

func (c *Core)HandleInIgnore(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInInvite(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInJoin(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInLeave(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInLike(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInListen(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInMove(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInOffer(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInQuestion(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInReject(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	// Figure out how to handle remotes
	// Need to know the type of object we are rejecting
	acptObj, err := c.db.GetObject(tx, act.ActObject)
	if err != nil {
		return err
	}
	switch acptObj.GetType() {
	case "Follow":
	}
	return nil
}

func (c *Core)HandleInRead(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInRemove(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInTentativeReject(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInTentativeAccept(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return c.HandleInAccept(tx, remoteP, act, subs)
}

func (c *Core)HandleInTravel(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInUndo(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInUpdate(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}

func (c *Core)HandleInView(tx *sql.Tx, remoteP bool, act *activitypub.Object, subs map[activitypub.HathiID]*activitypub.Object) (err error) {
	return nil
}
