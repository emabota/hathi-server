package activitypub

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	//"fmt"
)

var Logging bool

// All ActivityPub objects must implement this interface
type ActivityPub interface {
	GetType() string
	GetID() HathiID // This is the ActivityPub ID
	GetAtContext() string
	GetCore() ActivityPub
	Mapify() map[string]interface{}
	IsObject() bool // Distinguish between Objects and Links
	IsActor() bool
	IsActivity() bool
	IsIntransitiveActivity() bool
	IsCollection() bool
}

type HathiID string

func (h HathiID) Value() (driver.Value, error) {
	return string(h), nil
}

func (h *HathiID) Scan(value interface{}) error {
	if value == nil {
		*h = HathiID("")
	}
	if sv, err := driver.String.ConvertValue(value); err == nil {
		switch v := sv.(type) {
		case string:
			*h = HathiID(v)
		case []byte:
			*h = HathiID(v)
		default:
			return errors.New("failed to scan HathiID")
		}
	}
	return nil
}

type Presence []string

func (p *Presence)Contains(s string) (bool) {
	for _, v := range *p {
		if v == s {
			return true
		}
	}
	return false
}

func (p *Presence)Add(s string) {
	*p = append(*p, s)
}

func DecodeJSON(data []byte) (obj *Object, subs map[HathiID]*Object, err error) {
	// Tokenizes the raw JSON string into a dict which can be easily parsed,
	// then sends that dict to the actual decoder.
	var tmp interface{}
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return nil, nil, err
	}
	jsonData := tmp.(map[string]interface{})
	obj, subs, err = DecodeObjectMap(jsonData, "")
	if err != nil {
		return nil, nil, err
	}
	return obj, subs, nil
}

func DecodeObjectMap(data map[string]interface{}, tempIDRoot HathiID) (obj *Object, subs map[HathiID]*Object, err error) {
	// Parse the core object that all entities have
	obj, subs, err = DecodeObject(data, tempIDRoot)
	if err != nil {
		return nil, nil, err
	}
	return obj, subs, nil
}

func MapifyObjectWithEmbeds(obj ActivityPub, embeds map[HathiID]*Object) (encoded map[string]interface{}) {
	encoded = obj.Mapify()
	// List of the fields which can contain IDs
	// We don't just look through the keys in the encoded format because we
	// don't want to accidentally embed an object in a field that is never a
	// reference.
	keys := []string{
		"generator", "preview", "replies", "attachment", "attributedTo",
		"audience", "icon", "image", "inReplyTo", "url", "to", "bto", "cc",
		"bcc", "origin", "result", "object", "tag", "actor", "instrument",
		"target", "items", "current", "first", "last", "oneOf", "anyOf",
		"closed", "next", "prev", "partOf", "subject", "describes",
		"inbox", "outbox", "following", "followers", "expireTo",
	}
	// Embed finder / mapifier
	findE := func(search string, embeds map[HathiID]*Object) map[string]interface{} {
		for id, sub := range embeds {
			if string(id) == search {
				// Found an embed!
				return sub.Mapify()
			}
		}
		return nil
	}
	for _, field := range keys {
		if val, ok := encoded[field]; ok {
			// This field is present, look for an embed for this field
			switch typedVal := val.(type) {
			case string:
				enc := findE(typedVal, embeds)
				if enc != nil {
					encoded[field] = enc
				}
			case []string:
				newList := make([]interface{}, len(encoded[field].([]string)))
				for k, v := range typedVal {
					enc := findE(v, embeds)
					if enc != nil {
						newList[k] = enc
					} else {
						newList[k] = v
					}
				}
				encoded[field] = newList
			}
		}
	}
	return encoded
}
