package activitypub

import (
	"strconv"
	"time"
)

func handleDecodeIDList(dest *[]HathiID, source []interface{}, tmpID HathiID) (subs map[HathiID]*Object, err error) {
	// Handles decoding fields that are a list of objects
	var sub *Object
	var subs2 map[HathiID]*Object
	*dest = make([]HathiID, len(source))
	subs = make(map[HathiID]*Object)
	for i := range source {
		switch convSource := source[i].(type) {
		case string:
			(*dest)[i] = HathiID(convSource)
		case map[string]interface{}: // Must be a subobject
			sub, subs2, err = DecodeObjectMap(convSource,
				tmpID + HathiID(strconv.Itoa(i)))
			if err != nil {
				return nil, err
			}
			id := sub.GetID()
			(*dest)[i] = id
			subs[id] = sub
		}
	}
	for subID, sub := range subs2 {
		subs[subID] = sub
	}
	return subs, nil
}

func handleDecodeEmbeddedInList(subObj map[string]interface{}, tempID HathiID, subs map[HathiID]*Object, dest *[]HathiID) (err error) {
	*dest = append(*dest, "")
	return handleDecodeEmbedded(subObj, tempID, subs, &(*dest)[0])
}

func handleDecodeEmbedded(subObj map[string]interface{}, tempID HathiID, subs map[HathiID]*Object, dest *HathiID) (err error) {
	sub, subs2, err := DecodeObjectMap(subObj, tempID)
	if err != nil {
		return err
	}
	id := sub.GetID()
	subs[id] = sub
	*dest = id
	for subID, sub := range subs2 {
		subs[subID] = sub
	}
	return nil
}

func mapifyString(field string, s *string, target *map[string]interface{}) {
	if *s != "" {
		(*target)[field] = *s
	}
}

func mapifyID(field string, s *HathiID, target *map[string]interface{}) {
	if *s != "" {
		(*target)[field] = string(*s)
	}
}

func mapifyStringList(field string, lst *[]string, target *map[string]interface{}) {
	count := len(*lst)
	if count > 1 {
		newlst := make([]string, count)
		copy(newlst, *lst)
		(*target)[field] = newlst
	} else if count == 1 { // Single item lists are treated as scalars
		(*target)[field] = (*lst)[0]
	}
}

func mapifyIDList(field string, lst *[]HathiID, target *map[string]interface{}) {
	count := len(*lst)
	if count > 1 {
		newlst := make([]string, count)
		for i, v := range *lst {
			newlst[i] = string(v)
		}
		(*target)[field] = newlst
	} else if count == 1 { // Single item lists are treated as scalars
		(*target)[field] = string((*lst)[0])
	}
}

func mapifyLangMap(field string, m *map[string]string, target *map[string]interface{}) {
	if len(*m) > 0 {
		temp := make(map[string]string)
		for k, v := range (*m) {
			temp[k] = v
		}
		(*target)[field] = temp
	}
}

func handleLangMap(dest *map[string]string, source map[string]interface{}) (err error) {
	*dest = make(map[string]string)
	for k, v := range source {
		(*dest)[k] = v.(string)
	}
	return nil
}

// WARNING: The following are experimental and incomplete

func readFloat(dstr []byte, index int) (value float64, newIndex int, err error) {
	number := []byte{}
	if dstr[index] == '-' {
		number = append(number, '-')
		index++
	}
	hasDecimal := false
	dLen := len(dstr)
	for index < dLen {
		switch c := dstr[index]; c {
		case '0':
			fallthrough
		case '1':
			fallthrough
		case '2':
			fallthrough
		case '3':
			fallthrough
		case '4':
			fallthrough
		case '5':
			fallthrough
		case '6':
			fallthrough
		case '7':
			fallthrough
		case '8':
			fallthrough
		case '9':
			number = append(number, c)
		case '.':
			if hasDecimal == false {
				number = append(number, c)
				hasDecimal = true
			} else {
				// This shouldn't be here, let the code getter deal with it
				break
			}
		default: // Hit the end of the number
			break
		}
		index++
	}
	value, err = strconv.ParseFloat(string(number), 64)
	if err != nil {
		return 0.0, index, nil
	}
	return value, index, nil
}

func ParseISO8601Duration(dstr []byte) (duration time.Duration, err error) {
	index := 0
	dLen := len(dstr)
	units := map[string]float64{}
	inYear := true
	if dstr[0] != 'P' {
		// ERROR!
	}
	for index < dLen {
		// Read a number off
		value, index, err := readFloat(dstr, index)
		if err != nil {
			return time.Duration(0), err
		}
		// Read code Year/Month code
		switch c := dstr[index]; c {
		case 'Y':
			units["year"] = value
		case 'M':
			units["month"] = value
		case 'T':
			inYear = false
		default:
			break // Error
			}
			index++
		// parse number and store
		index++
	}
	inYear = inYear // shutup go
	return time.Duration(0), nil
}

func ValidateTimeTrio(startTime string, endTime string, duration string) {
	format := "2006-01-02T15:04:05.999999999Z07:00"
	count := 3
	_, err := time.Parse(format, startTime)
	if err != nil {
		count--
	}
	_, err = time.Parse(format, endTime)
	if err != nil {
		count--
	}
}
