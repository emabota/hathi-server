package activitypub

import (
	//"fmt"
)

var ActivityTypes = []string{
	"Activity",
	"Accept",
	"Add",
	"Announce",
	"Arrive",
	"Block",
	"Create",
	"Delete",
	"Dislike",
	"Flag",
	"Follow",
	"Ignore",
	"Invite",
	"Join",
	"Leave",
	"Like",
	"Listen",
	"Move",
	"Offer",
	"Question",
	"Reject",
	"Read",
	"Remove",
	"TentativeReject",
	"TentativeAccept",
	"Travel",
	"Undo",
	"Update",
	"View",
}

var ActorTypes = []string{
	"Actor",
	"Application",
	"Group",
	"Organization",
	"Person",
	"Service",
}

var CollectionTypes = []string{
	"Collection",
	"OrderedCollection",
	"Availability",
}

// strings are single-value, []strings are multi-value
// map[string]strings are language maps
type Object struct {
	ActivityPub
	// General strings / HathiIDs
	Type         string
	Id           HathiID
	AtContext    string
	Context      string
	EndTime      string
	Generator    HathiID
	Preview      HathiID
	Published    string
	Replies      HathiID
	StartTime    string
	Updated      string
	MediaType    string
	Duration     string
	FormerType   string
	Deleted      string
	// Activity strings / HathiIDs
	Result       HathiID
	Origin       HathiID
	ActObject    HathiID
	// Actor strings / HathiIDs
	Inbox        HathiID
	Outbox       HathiID
	Following    HathiID
	Followers    HathiID
	// TimeBlock strings / HathiIDs
	Expire       string // duration before startTime that this expires
	ExpireTo     HathiID // Availability or TimeBlock this expires to
	Availability string // What this block can be used for
	// General string / HathiID lists
	Attachment   []HathiID
	AttributedTo []HathiID
	Audience     []HathiID
	Icon         []HathiID
	Image        []HathiID
	InReplyTo    []HathiID
	Location     []HathiID
	Tag          []HathiID
	Url          []HathiID // This is actually wrong, need to implement Link
	To           []HathiID
	Bto          []HathiID
	Cc           []HathiID
	Bcc          []HathiID
	// Activity string / HathiID lists
	Actor        []HathiID
	Target       []HathiID
	Instrument   []HathiID
	// Collection HathiID list
	Items []HathiID
	// General langmaps
	Content      map[string]string
	Name         map[string]string
	Summary      map[string]string
	// General misc datatypes
	Altitude     float64
	// Other data, *not* JSONified
	Shadow       HathiID // The remote object this is a cache of
	PresenceBits Presence
}

func (o *Object)GetType() string {
	return o.Type
}

func (o *Object)IsObject() bool {
	return true
}

func (o *Object)IsActor() bool {
	for _, v := range ActorTypes {
		if v == o.Type {
			return true
		}
	}
	return false
}

func (o *Object)IsActivity() bool {
	for _, v := range ActivityTypes {
			if v == o.Type {
				return true
			}
	}
	return false
}

func (o *Object)IsIntransitiveActivity() bool {
	return false
}

func (o *Object)IsCollection() bool {
	for _, v := range CollectionTypes {
			if v == o.Type {
				return true
			}
	}
	return false
}

func (o *Object)GetID() HathiID {
	return o.Id
}

func (o *Object)GetAtContext() string {
	return o.AtContext
}

func (o *Object)GetCore() (obj ActivityPub) {
	return o
}

func DecodeObject(obj map[string]interface{}, tempIDRoot HathiID) (o *Object, subs map[HathiID]*Object, err error) {
	var subs2 map[HathiID]*Object
	o = new(Object)
	subs = make(map[HathiID]*Object)
	if tempIDRoot == "" {
		tempIDRoot = "^"
	}
	for field, v := range obj {
		switch typedV := v.(type) {
		case float64:
			switch field {
			case "altitude":
				o.Altitude = typedV
			}
			o.PresenceBits.Add(field)
		case string: // Simple fields
			switch field {
			case "type": // First group can only ever be strings
				o.Type = typedV
			case "@type":
				o.Type = typedV
			case "id":
				o.Id = HathiID(typedV)
			case "@id":
				o.Id = HathiID(typedV)
			case "@context":
				o.AtContext = typedV
			case "context":
				o.Context = typedV
			case "endTime":
				o.EndTime = typedV
			case "generator":
				o.Generator = HathiID(typedV)
			case "preview":
				o.Preview = HathiID(typedV)
			case "published":
				o.Published = typedV
			case "replies":
				o.Replies = HathiID(typedV)
			case "startTime":
				o.StartTime = typedV
			case "updated":
				o.Updated = typedV
			case "mediaType":
				o.MediaType = typedV
			case "duration":
				o.Duration = typedV
			case "formerType":
				o.FormerType = typedV
			case "deleted":
				o.Deleted = typedV
			case "origin": // From Activity
				o.Origin = HathiID(typedV)
			case "result": // From Activity
				o.Result = HathiID(typedV)
			case "object": // From Activity
				o.ActObject = HathiID(typedV)
			case "inbox": // From Actor
				o.Inbox = HathiID(typedV)
			case "outbox": // From Actor
				o.Outbox = HathiID(typedV)
			case "following": // From Actor
				o.Following = HathiID(typedV)
			case "followers": // From Actor
				o.Followers = HathiID(typedV)
			case "attachment": // These could be lists or strings
				o.Attachment = []HathiID{HathiID(typedV)}
			case "attributedTo":
				o.AttributedTo = []HathiID{HathiID(typedV)}
			case "audience":
				o.Audience = []HathiID{HathiID(typedV)}
			case "icon":
				o.Icon = []HathiID{HathiID(typedV)}
			case "image":
				o.Image = []HathiID{HathiID(typedV)}
			case "inReplyTo":
				o.InReplyTo = []HathiID{HathiID(typedV)}
			case "location":
				o.Location = []HathiID{HathiID(typedV)}
			case "tag":
				o.Tag = []HathiID{HathiID(typedV)}
			case "url":
				o.Url = []HathiID{HathiID(typedV)}
			case "to":
				o.To = []HathiID{HathiID(typedV)}
			case "bto":
				o.Bto = []HathiID{HathiID(typedV)}
			case "cc":
				o.Cc = []HathiID{HathiID(typedV)}
			case "bcc":
				o.Bcc = []HathiID{HathiID(typedV)}
			case "content": // These could be maps or strings
				o.Content = map[string]string{"en":typedV}
			case "name":
				o.Name = map[string]string{"en":typedV}
			case "summary":
				o.Summary = map[string]string{"en":typedV}
			case "target": // From Activity
				o.Target = []HathiID{HathiID(typedV)}
			case "actor": // From Activity
				o.Actor = []HathiID{HathiID(typedV)}
			case "instrument": // From Activity
				o.Instrument = []HathiID{HathiID(typedV)}
			case "items": // From Collection
				o.Items = []HathiID{HathiID(typedV)}
			case "expire": // From TimeBlock
				o.Expire = typedV
			case "expireTo": // From TimeBlock
				o.ExpireTo = HathiID(typedV)
			case "availability": // From TimeBlock
				o.Availability = typedV
			default:
				// Unknown field
			}
			if field == "type" || field == "id" {
				o.PresenceBits.Add("@" + field)
			} else {
				o.PresenceBits.Add(field)
			}
		case []interface{}: // Fields can be a list of strings or object-stubs
			switch field {
			case "attachment":
				subs2, err = handleDecodeIDList(&o.Attachment, typedV,
					tempIDRoot + "^attachment")
			case "attributedTo":
				subs2, err = handleDecodeIDList(&o.AttributedTo, typedV,
					tempIDRoot + "^attributedTo")
			case "audience":
				subs2, err = handleDecodeIDList(&o.Audience, typedV,
					tempIDRoot + "^audience")
			case "icon":
				subs2, err = handleDecodeIDList(&o.Icon, typedV,
					tempIDRoot + "^icon")
			case "image":
				subs2, err = handleDecodeIDList(&o.Image, typedV,
					tempIDRoot + "^image")
			case "inReplyTo":
				subs2, err = handleDecodeIDList(&o.InReplyTo, typedV,
					tempIDRoot + "^inReplyTo")
			case "location":
				subs2, err = handleDecodeIDList(&o.Location, typedV,
					tempIDRoot + "^location")
			case "tag":
				subs2, err = handleDecodeIDList(&o.Tag, typedV,
					tempIDRoot + "^tag")
			case "url":
				subs2, err = handleDecodeIDList(&o.Url, typedV,
					tempIDRoot + "^url")
			case "to":
				subs2, err = handleDecodeIDList(&o.To, typedV,
					tempIDRoot + "^to")
			case "bto":
				subs2, err = handleDecodeIDList(&o.Bto, typedV,
					tempIDRoot + "^bto")
			case "cc":
				subs2, err = handleDecodeIDList(&o.Cc, typedV,
					tempIDRoot + "^cc")
			case "bcc":
				subs2, err = handleDecodeIDList(&o.Bcc, typedV,
					tempIDRoot + "^bcc")
			case "target": // From Activity
				subs2, err = handleDecodeIDList(&o.Target, typedV,
					tempIDRoot + "^target")
			case "actor": // From Activity
				subs2, err = handleDecodeIDList(&o.Actor, typedV,
					tempIDRoot + "^actor")
			case "instrument": // From Activity
				subs2, err = handleDecodeIDList(&o.Instrument, typedV,
					tempIDRoot + "^instrument")
			case "items": // From Collection
				subs2, err = handleDecodeIDList(&o.Items, typedV,
					tempIDRoot + "^items")
			default:
				// Unknown field
			}
			o.PresenceBits.Add(field)
		case map[string]interface{}: // Fields are single subs or langmaps
			switch field { // Langmaps
			case "content":
				err = handleLangMap(&o.Content, typedV)
			case "name":
				err = handleLangMap(&o.Name, typedV)
			case "summary":
				err = handleLangMap(&o.Summary, typedV)
			case "attachment": // Sub-objects
				err = handleDecodeEmbeddedInList(typedV,
					tempIDRoot + "^attachment", subs, &o.Attachment)
			case "attributedTo":
				err = handleDecodeEmbeddedInList(typedV,
					tempIDRoot + "^attributedTo", subs, &o.AttributedTo)
			case "audience":
				err = handleDecodeEmbeddedInList(typedV,
					tempIDRoot + "^audience", subs, &o.Audience)
			case "generator":
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^generator",
					subs, &o.Generator)
			case "icon":
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^icon",
					subs, &o.Icon)
			case "image":
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^image",
					subs, &o.Image)
			case "inReplyTo":
				err = handleDecodeEmbeddedInList(typedV,
					tempIDRoot + "^inReplyTo", subs, &o.InReplyTo)
			case "location":
				err = handleDecodeEmbeddedInList(typedV,
					tempIDRoot + "^location", subs, &o.Location)
			case "preview":
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^preview",
					subs, &o.Preview)
			case "replies":
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^replies",
				subs, &o.Replies)
			case "tag":
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^tag",
					subs, &o.Tag)
			case "url":
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^url",
					subs, &o.Url)
			case "to":
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^to",
					subs, &o.To)
			case "bto":
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^bto",
					subs, &o.Bto)
			case "cc":
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^cc",
					subs, &o.Cc)
			case "bcc":
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^bcc",
					subs, &o.Bcc)
			case "origin": // From Activity
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^origin", subs,
					&o.Origin)
			case "result": // From Activity
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^result", subs,
					&o.Result)
			case "object": // From Activity
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^object", subs,
					&o.ActObject)
			case "target": // From Activity
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^target",
					subs, &o.Target)
			case "actor": // From Activity
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^actor",
					subs, &o.Actor)
			case "instrument": // From Activity
				err = handleDecodeEmbeddedInList(typedV,
					tempIDRoot + "^instrument", subs, &o.Instrument)
			case "inbox": // From Actor
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^inbox", subs,
					&o.Inbox)
			case "outbox": // From Actor
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^outbox", subs,
					&o.Outbox)
			case "following": // From Actor
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^following",
					subs, &o.Following)
			case "followers": // From Actor
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^followers",
					subs, &o.Followers)
			case "items": // From Collection
				err = handleDecodeEmbeddedInList(typedV, tempIDRoot + "^items",
					subs, &o.Items)
			case "expireTo": // From TimeBlock
				err = handleDecodeEmbedded(typedV, tempIDRoot + "^expireTo",
					subs, &o.ExpireTo)
			default:
				// Unknown field
			}
			o.PresenceBits.Add(field)
		}
		if err != nil {
			return nil, nil, err
		}
		for subID, sub := range subs2 {
			subs[subID] = sub
		}
	}
	if o.Id == "" { // Didn't get an ID
		// This is either a stub or a new object: place a temporary ID
		o.Id = tempIDRoot
	}
	return o, subs, nil
}

func (o *Object)Mapify() (mapped map[string]interface{}) {
	mapped = make(map[string]interface{})
	mapifyString("@type", &o.Type, &mapped)
	mapifyID("@id", &o.Id, &mapped)
	mapifyString("@context", &o.AtContext, &mapped)
	mapifyString("context", &o.Context, &mapped)
	mapifyString("endTime", &o.EndTime, &mapped)
	mapifyID("generator", &o.Generator, &mapped)
	mapifyID("preview", &o.Preview, &mapped)
	mapifyString("published", &o.Published, &mapped)
	mapifyID("replies", &o.Replies, &mapped)
	mapifyString("startTime", &o.StartTime, &mapped)
	mapifyString("updated", &o.Updated, &mapped)
	mapifyString("mediaType", &o.MediaType, &mapped)
	mapifyString("duration", &o.Duration, &mapped)
	mapifyString("formerType", &o.FormerType, &mapped)
	mapifyString("deleted", &o.Deleted, &mapped)
	mapifyID("result", &o.Result, &mapped)
	mapifyID("origin", &o.Origin, &mapped)
	mapifyID("object", &o.ActObject, &mapped)
	mapifyIDList("attachment", &o.Attachment, &mapped)
	mapifyIDList("attributedTo", &o.AttributedTo, &mapped)
	mapifyIDList("audience", &o.Audience, &mapped)
	mapifyIDList("icon", &o.Icon, &mapped)
	mapifyIDList("image", &o.Image, &mapped)
	mapifyIDList("inReplyTo", &o.InReplyTo, &mapped)
	mapifyIDList("location", &o.Location, &mapped)
	mapifyIDList("tag", &o.Tag, &mapped)
	mapifyIDList("url", &o.Url, &mapped)
	mapifyIDList("to", &o.To, &mapped)
	mapifyIDList("bto", &o.Bto, &mapped)
	mapifyIDList("cc", &o.Cc, &mapped)
	mapifyIDList("bcc", &o.Bcc, &mapped)
	mapifyIDList("actor", &o.Actor, &mapped)
	mapifyIDList("target", &o.Target, &mapped)
	mapifyIDList("instrument", &o.Instrument, &mapped)
	mapifyID("inbox", &o.Inbox, &mapped)
	mapifyID("outbox", &o.Outbox, &mapped)
	mapifyID("following", &o.Following, &mapped)
	mapifyID("followers", &o.Followers, &mapped)
	mapifyIDList("items", &o.Items, &mapped)
	mapifyLangMap("content", &o.Content, &mapped)
	mapifyLangMap("name", &o.Name, &mapped)
	mapifyLangMap("summary", &o.Summary, &mapped)
	mapped["altitude"] = o.Altitude
	// TimeBlock
	mapifyString("expire", &o.Expire, &mapped)
	mapifyID("expireTo", &o.ExpireTo, &mapped)
	mapifyString("availability", &o.Availability, &mapped)
	return mapped
}
