package activitypub

import (
	"encoding/json"
	//"fmt"
	"testing"
	"reflect"
)

func TestObjectDecode(t *testing.T) {
	// Test all simple fields
	// ======================================================
	data := map[string]interface{}{
		"type":"objtype", "id":"hathi.rapture.test", "@context":"nowhere",
		"context":"out_of", "endTime":"tomorrow", "generator":"reactor0",
		"preview":"palantir", "published":"never", "replies":"none",
		"startTime":"yesterday", "updated":"today", "mediaType":"text",
		"duration":"too_long", "formerType":"parrot", "deleted":"forever",
		"attachment":"plan.txt", "attributedTo":"my_muse", "audience":"fools",
		"icon":"icon.ico", "image":"avatarofdoom.png", "inReplyTo":"no_one",
		"location":"fort_frolic", "tag":"genius", "url":"www.masterpiece.test",
		"to":"legacy", "bto":"pyre", "cc":"doubters", "bcc":"ocean",
		"content":"betrayers", "name":"quadtych", "summary":"life's_work",
		"altitude":15.0,
		// From Activity
		"origin":"T-0", "result":"everything", "object":"the_work",
		"target":"multi", "actor":"thespian", "instrument":"will",
	}
	obj, subs, err := DecodeObject(data, "")
	if err != nil {
		t.Fatal("Test DecodeObject failure:", err)
	}
	if len(subs) != 0 {
		t.Fatal("Test DecodeObject: too many subs:", subs)
	}
	if obj.Type != "objtype" || obj.Id != "hathi.rapture.test" ||
		obj.AtContext != "nowhere" || obj.Context != "out_of" ||
		obj.EndTime != "tomorrow" || obj.Generator != "reactor0" ||
		obj.Preview != "palantir" || obj.Published != "never" ||
		obj.Replies != "none" || obj.StartTime != "yesterday" ||
		obj.Updated != "today" || obj.MediaType != "text" ||
		obj.Duration != "too_long" || obj.FormerType != "parrot" ||
		obj.Deleted != "forever" || obj.Altitude != 15.0 ||
		obj.Origin != "T-0" || obj.Result != "everything" ||
		obj.ActObject != "the_work" ||
		reflect.DeepEqual(obj.Attachment, []HathiID{"plan.txt"}) != true ||
		reflect.DeepEqual(obj.AttributedTo, []HathiID{"my_muse"}) != true ||
		reflect.DeepEqual(obj.Audience, []HathiID{"fools"}) != true ||
		reflect.DeepEqual(obj.Icon, []HathiID{"icon.ico"}) != true ||
		reflect.DeepEqual(obj.Image, []HathiID{"avatarofdoom.png"}) != true ||
		reflect.DeepEqual(obj.InReplyTo, []HathiID{"no_one"}) != true ||
		reflect.DeepEqual(obj.Location, []HathiID{"fort_frolic"}) != true ||
		reflect.DeepEqual(obj.Tag, []HathiID{"genius"}) != true ||
		reflect.DeepEqual(obj.Url, []HathiID{"www.masterpiece.test"}) != true ||
		reflect.DeepEqual(obj.To, []HathiID{"legacy"}) != true ||
		reflect.DeepEqual(obj.Bto, []HathiID{"pyre"}) != true ||
		reflect.DeepEqual(obj.Cc,  []HathiID{"doubters"}) != true ||
		reflect.DeepEqual(obj.Bcc, []HathiID{"ocean"}) != true ||
		reflect.DeepEqual(obj.Target, []HathiID{"multi"}) != true ||
		reflect.DeepEqual(obj.Actor, []HathiID{"thespian"}) != true ||
		reflect.DeepEqual(obj.Instrument, []HathiID{"will"}) != true ||
		reflect.DeepEqual(obj.Content, map[string]string{"en":"betrayers"}) != true ||
		reflect.DeepEqual(obj.Name, map[string]string{"en":"quadtych"}) != true ||
		reflect.DeepEqual(obj.Summary, map[string]string{"en":"life's_work"}) != true {
		t.Fatal("Test DecodeObject: equality failure:", obj)
	}
	// Test string lists
	// ======================================================
	data["attachment"] = []interface{}{"miasma", "quadtych"}
	data["actor"] = []interface{}{"actor", "actress"}
	data["attributedTo"] = []interface{}{"parasites", "doubters"}
	data["audience"] = []interface{}{"self", "thinkers"}
	data["icon"] = []interface{}{"key.ico", "code.ico"}
	data["image"] = []interface{}{"iceman", "cometh"}
	data["inReplyTo"] = []interface{}{"none", "everyone"}
	data["location"] = []interface{}{"fleet_hall", "theater"}
	data["tag"] = []interface{}{"revenge", "art"}
	data["url"] = []interface{}{"art.cohen.test", "art.rabbits.test"}
	data["to"] = []interface{}{"muse", "legacy"}
	data["bto"] = []interface{}{"nothing", "never"}
	data["cc"] = []interface{}{"jack", "songbird"}
	data["bcc"] = []interface{}{"sally", "MVP"}
	obj, subs, err = DecodeObject(data, "")
	if err != nil {
		t.Fatal("Test DecodeObject failure:", err)
	}
	if len(subs) != 0 {
		t.Fatal("Test DecodeObject: too many subs:", subs)
	}
	if reflect.DeepEqual(obj.Attachment, []HathiID{"miasma", "quadtych"}) != true ||
		reflect.DeepEqual(obj.AttributedTo, []HathiID{"parasites", "doubters"}) != true ||
		reflect.DeepEqual(obj.Audience, []HathiID{"self", "thinkers"}) != true ||
		reflect.DeepEqual(obj.Icon, []HathiID{"key.ico", "code.ico"}) != true ||
		reflect.DeepEqual(obj.Image, []HathiID{"iceman", "cometh"}) != true ||
		reflect.DeepEqual(obj.InReplyTo, []HathiID{"none", "everyone"}) != true ||
		reflect.DeepEqual(obj.Location, []HathiID{"fleet_hall", "theater"}) != true ||
		reflect.DeepEqual(obj.Tag, []HathiID{"revenge", "art"}) != true ||
		reflect.DeepEqual(obj.Url, []HathiID{"art.cohen.test", "art.rabbits.test"}) != true ||
		reflect.DeepEqual(obj.To, []HathiID{"muse", "legacy"}) != true ||
		reflect.DeepEqual(obj.Bto, []HathiID{"nothing", "never"}) != true ||
		reflect.DeepEqual(obj.Cc, []HathiID{"jack", "songbird"}) != true ||
		reflect.DeepEqual(obj.Bcc, []HathiID{"sally", "MVP"}) != true {
		t.Fatal("Test DecodeObject: equality failure:", obj)
	}
	// Test sub-object lists (mixed with normal strings)
	// ======================================================
	data["attachment"] = []interface{}{
		map[string]interface{}{"type": "Note", "id":"foo"}, "miasma",
	}
	data["actor"] = []interface{}{
		map[string]interface{}{"type": "Note"}, "actor",
	}
	data["attributedTo"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "parasites",
	}
	data["audience"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "self",
	}
	data["icon"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "key.ico",
	}
	data["image"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "iceman",
	}
	data["inReplyTo"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "none",
	}
	data["location"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "fleet_hall",
	}
	data["tag"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "revenge",
	}
	data["url"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "art.cohen.test",
	}
	data["to"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "muse",
	}
	data["bto"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "nothing",
	}
	data["cc"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "jack",
	}
	data["bcc"] = []interface{}{
		map[string]interface{}{"type":"Note"}, "sally",
	}
	obj, subs, err = DecodeObject(data, "")
	if err != nil {
		t.Fatal("Test DecodeObject failure:", err)
	}
	if len(subs) != 14 {
		t.Fatal("Test DecodeObject: incorrect subs count:", len(subs))
	}
	if reflect.DeepEqual(obj.Attachment, []HathiID{"foo", "miasma"}) != true ||
		reflect.DeepEqual(obj.AttributedTo, []HathiID{"^^attributedTo0", "parasites"}) != true ||
		reflect.DeepEqual(obj.Audience, []HathiID{"^^audience0", "self"}) != true ||
		reflect.DeepEqual(obj.Icon, []HathiID{"^^icon0", "key.ico"}) != true ||
		reflect.DeepEqual(obj.Image, []HathiID{"^^image0", "iceman"}) != true ||
		reflect.DeepEqual(obj.InReplyTo, []HathiID{"^^inReplyTo0", "none"}) != true ||
		reflect.DeepEqual(obj.Location, []HathiID{"^^location0", "fleet_hall"}) != true ||
		reflect.DeepEqual(obj.Tag, []HathiID{"^^tag0", "revenge"}) != true ||
		reflect.DeepEqual(obj.Url, []HathiID{"^^url0", "art.cohen.test"}) != true ||
		reflect.DeepEqual(obj.To, []HathiID{"^^to0", "muse"}) != true ||
		reflect.DeepEqual(obj.Bto, []HathiID{"^^bto0", "nothing"}) != true ||
		reflect.DeepEqual(obj.Cc,  []HathiID{"^^cc0", "jack"}) != true ||
		reflect.DeepEqual(obj.Bcc, []HathiID{"^^bcc0", "sally"}) != true {
		t.Fatal("Test DecodeObject: incorrect IDs:", obj)
	}
	// Test langmaps
	// ======================================================
	data["content"] = map[string]interface{}{"en":"content", "de":"inhalt"}
	data["name"] = map[string]interface{}{"en":"name", "de":"name"}
	data["summary"] = map[string]interface{}{
		"en":"summary", "de":"Zusammenfassung"}
	obj, subs, err = DecodeObject(data, "")
	if err != nil {
		t.Fatal("Test DecodeObject failure:", err)
	}
	if reflect.DeepEqual(obj.Content, map[string]string{"en":"content", "de":"inhalt"}) != true ||
		reflect.DeepEqual(obj.Name, map[string]string{"en":"name", "de":"name"}) != true ||
		reflect.DeepEqual(obj.Summary, map[string]string{"en":"summary", "de":"Zusammenfassung"}) != true {
		t.Fatal("Test DecodeObject: incorrect langmaps:", obj)
	}
	// Test single sub-objects
	// ======================================================
	data["attachment"] = map[string]interface{}{"type": "Note", "id":"foo"}
	data["attributedTo"] = map[string]interface{}{"type":"Note"}
	data["audience"] = map[string]interface{}{"type":"Note"}
	data["generator"] = map[string]interface{}{"type":"Note"}
	data["icon"] = map[string]interface{}{"type":"Note"}
	data["image"] = map[string]interface{}{"type":"Note"}
	data["inReplyTo"] = map[string]interface{}{"type":"Note"}
	data["location"] = map[string]interface{}{"type":"Note"}
	data["preview"] = map[string]interface{}{"type":"Note"}
	data["replies"] = map[string]interface{}{"type":"Note"}
	data["tag"] = map[string]interface{}{"type":"Note"}
	data["url"] = map[string]interface{}{"type":"Note"}
	data["to"] = map[string]interface{}{"type":"Note"}
	data["bto"] = map[string]interface{}{"type":"Note"}
	data["cc"] = map[string]interface{}{"type":"Note"}
	data["bcc"] = map[string]interface{}{"type":"Note"}
	obj, subs, err = DecodeObject(data, "")
	if err != nil {
		t.Fatal("Test DecodeObject failure:", err)
	}
	if len(subs) != 17 {
		t.Fatal("Test DecodeObject: incorrect subs count:", len(subs), subs)
	}
	if reflect.DeepEqual(obj.Attachment, []HathiID{"foo"}) != true ||
		reflect.DeepEqual(obj.AttributedTo, []HathiID{"^^attributedTo"}) != true ||
		reflect.DeepEqual(obj.Audience, []HathiID{"^^audience"}) != true ||
		reflect.DeepEqual(obj.Icon, []HathiID{"^^icon"}) != true ||
		reflect.DeepEqual(obj.Image, []HathiID{"^^image"}) != true ||
		reflect.DeepEqual(obj.InReplyTo, []HathiID{"^^inReplyTo"}) != true ||
		reflect.DeepEqual(obj.Location, []HathiID{"^^location"}) != true ||
		reflect.DeepEqual(obj.Tag, []HathiID{"^^tag"}) != true ||
		reflect.DeepEqual(obj.Url, []HathiID{"^^url"}) != true ||
		reflect.DeepEqual(obj.To, []HathiID{"^^to"}) != true ||
		reflect.DeepEqual(obj.Bto, []HathiID{"^^bto"}) != true ||
		reflect.DeepEqual(obj.Cc,  []HathiID{"^^cc"}) != true ||
		reflect.DeepEqual(obj.Bcc, []HathiID{"^^bcc"}) != true {
		t.Fatal("Test DecodeObject: incorrect IDs:", obj)
	}
	if obj.Generator != "^^generator" || obj.Preview != "^^preview" ||
		obj.Replies != "^^replies" {
		t.Fatal("Test DecodeObject: incorrect IDs:", obj)
	}
}

func TestObjectMapify(t *testing.T) {
	obj := Object{
		Type:"Note", Id:"foo", AtContext:"@bar", Context:"None",
		EndTime:"Heat Death", Generator:"Atomic", Preview:"What is to come",
		Published:"Prestigious", Replies:"Prompt", StartTime:"T=0",
		Updated:"Never", MediaType:"New", Duration:"Forever",
		FormerType:"parrot", Deleted:"forever", Attachment:[]HathiID{"blah"},
		AttributedTo:[]HathiID{"Causes", "Effects"}, Audience:[]HathiID{"Woke"},
		Icon:[]HathiID{"ography"}, Image:[]HathiID{"Enhanced"},
		InReplyTo:[]HathiID{"Reality"}, Location:[]HathiID{"Sol"},
		Tag:[]HathiID{"You're it!"}, Url:[]HathiID{"derp.test"},
		To:[]HathiID{"Nowhere"}, Bto:[]HathiID{"Somewhere"},
		Cc:[]HathiID{"Da Man"}, Bcc:[]HathiID{"Spooks"},
		Content:map[string]string{"en":"So is it contained"},
		Name:map[string]string{"en":"Alpha", "de":"Omega"},
		Summary:map[string]string{"en":"None"}, Altitude:42.0,
	}
	mapped := obj.Mapify()
	data, err := json.Marshal(mapped)
	if err != nil {
		t.Fatal("Object Mapify marshal failure:", err)
	}
	newObj, _, err := DecodeJSON(data)
	if err != nil {
		t.Fatal("Object Mapify decode failure:", err)
	}
	if newObj.Type != obj.Type {
		t.Fatal("Object Mapify Type failure:", obj.Type, newObj.Type)
	} else if newObj.Id != obj.Id {
		t.Fatal("Object Mapify Id failure:", obj.Id, newObj.Id)
	} else if newObj.AtContext != obj.AtContext {
		t.Fatal("Object Mapify AtContext failure:",
			obj.AtContext, newObj.AtContext)
	} else if newObj.Context != obj.Context {
		t.Fatal("Object Mapify Context failure:", obj.Context, newObj.Context)
	} else if newObj.EndTime != obj.EndTime {
		t.Fatal("Object Mapify EndTime failure:", obj.EndTime, newObj.EndTime)
	} else if newObj.Generator != obj.Generator {
		t.Fatal("Object Mapify Generator failure:",
			obj.Generator, newObj.Generator)
	} else if newObj.Preview != obj.Preview {
		t.Fatal("Object Mapify Preview failure:", obj.Preview, newObj.Preview)
	} else if newObj.Published != obj.Published {
		t.Fatal("Object Mapify Published failure:",
			obj.Published, newObj.Published)
	} else if newObj.Replies != obj.Replies {
		t.Fatal("Object Mapify Replies failure:", obj.Replies, newObj.Replies)
	} else if newObj.StartTime != obj.StartTime {
		t.Fatal("Object Mapify StartTime failure:",
			obj.StartTime, newObj.StartTime)
	} else if newObj.Updated != obj.Updated {
		t.Fatal("Object Mapify Updated failure:", obj.Updated, newObj.Updated)
	} else if newObj.MediaType != obj.MediaType {
		t.Fatal("Object Mapify MediaType failure:",
			obj.MediaType, newObj.MediaType)
	} else if newObj.Duration != obj.Duration {
		t.Fatal("Object Mapify Duration failure:",
			obj.Duration, newObj.Duration)
	} else if newObj.FormerType != obj.FormerType {
		t.Fatal("Object Mapify FormerType failure:",
			obj.FormerType, newObj.FormerType)
	} else if newObj.Deleted != obj.Deleted {
		t.Fatal("Object Mapify Deleted failure:",
			obj.Deleted, newObj.Deleted)
	} else if !reflect.DeepEqual(newObj.Attachment, obj.Attachment) {
		t.Fatal("Object Mapify Attachment failure:",
			obj.Attachment, newObj.Attachment)
	} else if !reflect.DeepEqual(newObj.AttributedTo, obj.AttributedTo) {
		t.Fatal("Object Mapify AttributedTo failure:",
			obj.AttributedTo, newObj.AttributedTo)
	} else if !reflect.DeepEqual(newObj.Audience, obj.Audience) {
		t.Fatal("Object Mapify Audience failure:",
			obj.Audience, newObj.Audience)
	} else if !reflect.DeepEqual(newObj.Icon, obj.Icon) {
		t.Fatal("Object Mapify Icon failure:", obj.Icon, newObj.Icon)
	} else if !reflect.DeepEqual(newObj.Image, obj.Image) {
		t.Fatal("Object Mapify Image failure:", obj.Image, newObj.Image)
	} else if !reflect.DeepEqual(newObj.InReplyTo, obj.InReplyTo) {
		t.Fatal("Object Mapify InReplyTo failure:",
			obj.InReplyTo, newObj.InReplyTo)
	} else if !reflect.DeepEqual(newObj.Location, obj.Location) {
		t.Fatal("Object Mapify Location failure:",
			obj.Location, newObj.Location)
	} else if !reflect.DeepEqual(newObj.Tag, obj.Tag) {
		t.Fatal("Object Mapify Tag failure:", obj.Tag, newObj.Tag)
	} else if !reflect.DeepEqual(newObj.Url, obj.Url) {
		t.Fatal("Object Mapify Url failure:", obj.Url, newObj.Url)
	} else if !reflect.DeepEqual(newObj.To, obj.To) {
		t.Fatal("Object Mapify To failure:", obj.To, newObj.To)
	} else if !reflect.DeepEqual(newObj.Bto, obj.Bto) {
		t.Fatal("Object Mapify Bto failure:", obj.Bto, newObj.Bto)
	} else if !reflect.DeepEqual(newObj.Cc, obj.Cc) {
		t.Fatal("Object Mapify Cc failure:", obj.Cc, newObj.Cc)
	} else if !reflect.DeepEqual(newObj.Bcc, obj.Bcc) {
		t.Fatal("Object Mapify Bcc failure:", obj.Bcc, newObj.Bcc)
	} else if !reflect.DeepEqual(newObj.Content, obj.Content) {
		t.Fatal("Object Mapify Content failure:", obj.Content, newObj.Content)
	} else if !reflect.DeepEqual(newObj.Name, obj.Name) {
		t.Fatal("Object Mapify Name failure:", obj.Name, newObj.Name)
	} else if !reflect.DeepEqual(newObj.Summary, obj.Summary) {
		t.Fatal("Object Mapify Summary failure:", obj.Summary, newObj.Summary)
	}
}
