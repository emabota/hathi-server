package activitypub

import (
	//"fmt"
	"testing"
	//"reflect"
	//"encoding/json"
)

func TestPresence(t *testing.T) {
	var p Presence
	p.Add("foo")
	if len(p) != 1 {
		t.Fatal("Test presence incorrect length", p)
	}
	if p.Contains("foo") != true {
		t.Fatal("Test presence does not contain \"foo\"", p)
	}
	if p.Contains("bar") != false {
		t.Fatal("Test presence should not contain \"bar\"", p)
	}
}

func TestMapifyObjectWithEmbeds(t *testing.T) {
	// Setup
	obj := Object{Id:"foo", Type:"Note", Content:map[string]string{"en":"blah"},
		Replies:"where/who", Preview:"where/future",
		To:[]HathiID{"who/one", "who/two"}}
	embedPreview := Object{Id:"where/future", Type:"Note"}
	embedTo := Object{Id:"who/one", Type:"Note"}
	embeds := map[HathiID]*Object{"where/future":&embedPreview,
		"who/one":&embedTo}
	// Run test
	encoded := MapifyObjectWithEmbeds(&obj, embeds)
	//tmp, _ := json.Marshal(encoded)
	//fmt.Println(string(tmp))
	// Check root object
	if len(encoded) != 7 {
		t.Fatal("Test MapifyObjectWithEmbeds wrong length", len(encoded))
	}
	if encoded["@id"].(string) != "foo" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong id",
			encoded["id"].(string))
	}
	if encoded["@type"].(string) != "Note" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong type",
			encoded["type"].(string))
	}
	if encoded["replies"].(string) != "where/who" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong replies",
			encoded["replies"].(string))
	}
	if encoded["altitude"].(float64) != 0 {
		t.Fatal("Test MapifyObjectWithEmbeds wrong altitude",
			encoded["altitude"].(float64))
	}
	if encoded["content"].(map[string]string)["en"] != "blah" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong content[en]",
			encoded["content"].(map[string]string)["en"])
	}
	// Check embedded preview object
	preview := encoded["preview"].(map[string]interface{})
	if preview["altitude"].(float64) != 0 {
		t.Fatal("Test MapifyObjectWithEmbeds wrong preview.altitude",
			preview["altitude"])
	}
	if preview["@id"].(string) != "where/future" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong preview.id",
			preview["id"])
	}
	if preview["@type"].(string) != "Note" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong preview.type",
			preview["type"])
	}
	// Check to field
	to := encoded["to"].([]interface{})
	if to[1].(string) != "who/two" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong to[1]", to[1].(string))
	}
	if to[0].(map[string]interface{})["altitude"].(float64) != 0 {
		t.Fatal("Test MapifyObjectWithEmbeds wrong to[0].altitude",
			to[0].(map[string]interface{})["altitude"])
	}
	if to[0].(map[string]interface{})["@id"].(string) != "who/one" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong to[0].id",
			to[0].(map[string]interface{})["id"].(string))
	}
	if to[0].(map[string]interface{})["@type"].(string) != "Note" {
		t.Fatal("Test MapifyObjectWithEmbeds wrong to[0].type",
			to[0].(map[string]interface{})["type"].(string))
	}
}
