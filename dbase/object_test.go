package dbase

import (
	//"fmt"
	"reflect"
	"testing"

	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestDatabaseObject(t *testing.T) {
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	obj := activitypub.Object{
		Type: "Test", Id: "foo", AtContext: "@bar",
		Attachment:   []activitypub.HathiID{"one", "two"},
		AttributedTo: []activitypub.HathiID{"three", "four"},
		Audience:     []activitypub.HathiID{"five", "six"},
		Content:      map[string]string{"en": "No", "de": "Nein"},
		Name:         map[string]string{"en": "John", "de": "Johan"},
		Context:      "contextual", EndTime: "12/31/2036",
		Generator: "blah", Icon: []activitypub.HathiID{"seven", "eight"},
		Image:     []activitypub.HathiID{"nine", "ten"},
		InReplyTo: []activitypub.HathiID{"eleven", "twelve"},
		Location:  []activitypub.HathiID{"thirteen", "fourteen"},
		Preview: "baz", Published: "Quux", Replies: "replies.box",
		StartTime: "now",
		Summary: map[string]string{"en": "ding", "de": "dong"},
		Tag:     []activitypub.HathiID{"fifteen", "sixteen"}, Updated: "never",
		Url:       []activitypub.HathiID{"seventeen", "eighteen"},
		To:        []activitypub.HathiID{"nineteen", "twenty"},
		Bto:       []activitypub.HathiID{"twenty-one", "twenty-two"},
		Cc:        []activitypub.HathiID{"twenty-three", "twenty-four"},
		Bcc:       []activitypub.HathiID{"twenty-five", "twenty-six"},
		MediaType: "text", Duration: "0:00", FormerType: "ex-",
		Deleted: "forever", Shadow: "shade", ActObject: "upon",
		Result: "entropy", Origin: "nothing",
		Actor: []activitypub.HathiID{"actOne", "actTwo"},
		Target: []activitypub.HathiID{"tarOne", "tarTwo"},
		Instrument: []activitypub.HathiID{"insOne", "insTwo"},
		Inbox: "in", Outbox: "out", Following: "fwing", Followers: "fwers",
		Items: []activitypub.HathiID{"1", "2", "3"}, Expire: "then",
		ExpireTo: "other", Availability: "None",
	}

	// ===================================
	// ObjectSet
	// ===================================
	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from OBJECTS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("delete from REFS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("delete from LANGMAP").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("delete from ITEMS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("insert into OBJECTS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("Test", "foo", "contextual", "@bar", "12/31/2036", "blah",
		"baz", "Quux", "replies.box", "now", "never", "text", "0:00", "ex-",
		"forever", "shade", "upon", "entropy", "nothing", "in", "out", "fwing",
		"fwers", "then", "other", "None")
	// Refs table
	refExpected := [][]string{
		{"attachment", "one"}, {"attachment", "two"},
		{"attributedTo", "three"}, {"attributedTo", "four"},
		{"audience", "five"}, {"audience", "six"},
		{"icon", "seven"}, {"icon", "eight"},
		{"image", "nine"}, {"image", "ten"},
		{"inReplyTo", "eleven"}, {"inReplyTo", "twelve"},
		{"location", "thirteen"}, {"location", "fourteen"},
		{"tag", "fifteen"}, {"tag", "sixteen"},
		{"to", "nineteen"}, {"to", "twenty"},
		{"bto", "twenty-one"}, {"bto", "twenty-two"},
		{"cc", "twenty-three"}, {"cc", "twenty-four"},
		{"bcc", "twenty-five"}, {"bcc", "twenty-six"},
		{"url", "seventeen"}, {"url", "eighteen"},
		{"actor", "actOne"}, {"actor", "actTwo"},
		{"target", "tarOne"}, {"target", "tarTwo"},
		{"instrument", "insOne"}, {"instrument", "insTwo"},
	}
	for _, v := range refExpected {
		mock.ExpectExec("insert into REFS").
			WillReturnResult(sqlmock.NewResult(0, 0)).
			WithArgs("foo", v[0], v[1])
	}
	// Langs table
	langExpected := [][]string{
		{"name", "de", "Johan"},
		{"name", "en", "John"},
		{"content", "de", "Nein"},
		{"content", "en", "No"},
		{"summary", "de", "dong"},
		{"summary", "en", "ding"},
	}
	for _, v := range langExpected {
		mock.ExpectExec("insert into LANGMAP").
			WillReturnResult(sqlmock.NewResult(0, 0)).
			WithArgs("foo", v[0], v[1], v[2])
	}
	// Items
	itemsExpected := []string{"1", "2", "3"}
	for i, v := range itemsExpected {
		mock.ExpectExec("insert into ITEMS").
			WillReturnResult(sqlmock.NewResult(0, 0)).
			WithArgs("foo", i, v)
	}
	mock.ExpectCommit()

	// Do test
	err = db.SetObject(nil, &obj)
	if err != nil {
		t.Fatal("Database ObjectSet:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database ObjectSet: unfulfilled expectations:", err)
	}

	// ===============================
	// ObjectGet
	// ===============================
	// Set up expectations
	mock.ExpectBegin()
	// Object table
	objRows := sqlmock.NewRows([]string{"TYPE", "ID", "CONTEXT", "ATCONTEXT",
		"ENDTIME", "GENERATOR", "PREVIEW", "PUBLISHED", "REPLIES", "STARTTIME",
		"UPDATED", "MEDIATYPE", "DURATION", "FORMERTYPE", "DELETED", "SHADOW",
		"ACTOBJECT", "RESULT", "ORIGIN", "INBOX", "OUTBOX", "FOLLOWING",
		"FOLLOWERS", "EXPIRE", "EXPIRETO", "AVAILABILITY"}).
			AddRow("Test", "foo", "contextual", "@bar", "12/31/2036", "blah",
			"baz", "Quux", "replies.box", "now", "never", "text", "0:00", "ex-",
			"forever", "shade", "upon", "entropy", "nothing", "in", "out",
			"fwing", "fwers", "then", "other", "None")
	mock.ExpectQuery(
		"select TYPE, ID, CONTEXT, ATCONTEXT, ENDTIME, GENERATOR, PREVIEW, PUBLISHED, REPLIES, STARTTIME, UPDATED, MEDIATYPE, DURATION, FORMERTYPE, DELETED, SHADOW, ACTOBJECT, RESULT, ORIGIN, INBOX, OUTBOX, FOLLOWING, FOLLOWERS, EXPIRE, EXPIRETO, AVAILABILITY from OBJECTS").
			WithArgs("foo").
			WillReturnRows(objRows)
	// Refs table
	refsExpected := [][]string{
		{"attachment", "one", "two"},
		{"attributedTo", "three", "four"},
		{"audience", "five", "six"},
		{"icon", "seven", "eight"},
		{"image", "nine", "ten"},
		{"inReplyTo", "eleven", "twelve"},
		{"location", "thirteen", "fourteen"},
		{"tag", "fifteen", "sixteen"},
		{"to", "nineteen", "twenty"},
		{"bto", "twenty-one", "twenty-two"},
		{"cc", "twenty-three", "twenty-four"},
		{"bcc", "twenty-five", "twenty-six"},
		{"url", "seventeen", "eighteen"},
		{"actor", "actOne", "actTwo"},
		{"target", "tarOne", "tarTwo"},
		{"instrument", "insOne", "insTwo"},
	}
	for _, row := range refsExpected {
		refRows := sqlmock.NewRows([]string{"REFID"}).
			AddRow(row[1]).
			AddRow(row[2])
		mock.ExpectQuery("select REFID from REFS").
			WithArgs("foo", row[0]).
			WillReturnRows(refRows)
	}
	// Langmap table
	langsExpected := [][]string{
		{"name", "de", "Johan", "en", "John"},
		{"content", "de", "Nein", "en", "No"},
		{"summary", "de", "dong", "en", "ding"},
	}
	for _, row := range langsExpected {
		langRows := sqlmock.NewRows([]string{"LANG", "DATA"}).
			AddRow(row[1], row[2]).
			AddRow(row[3], row[4])
		mock.ExpectQuery("select LANG, DATA from LANGMAP").
			WithArgs("foo", row[0]).
			WillReturnRows(langRows)
	}
	// Items
	itemRows := sqlmock.NewRows([]string{"ITEMID"}).
		AddRow("1").
		AddRow("2").
		AddRow("3")
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Do test
	nObj, err := db.GetObject(nil, "foo")
	if err != nil {
		t.Fatal("Database ObjectGet:", err)
	}
	if nObj == nil {
		t.Fatal("Database ObjectGet: newObj is nil")
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database ObjectGet: unfulfilled expectations:", err)
	}
	// Test retrieved opject
	// Test simple
	if nObj.Type != "Test" || nObj.Id != "foo" || nObj.AtContext != "@bar" ||
		nObj.Context != "contextual" || nObj.EndTime != "12/31/2036" ||
		nObj.Generator != "blah" || nObj.Preview != "baz" ||
		nObj.Published != "Quux" || nObj.Replies != "replies.box" ||
		nObj.StartTime != "now" || nObj.Updated != "never" ||
		nObj.MediaType != "text" || nObj.Duration != "0:00" ||
		nObj.FormerType != "ex-" || nObj.Deleted != "forever" ||
		nObj.Shadow != "shade" || nObj.ActObject != "upon" ||
		nObj.Result != "entropy" || nObj.Origin != "nothing" ||
		nObj.Inbox != "in" || nObj.Outbox != "out" ||
		nObj.Following != "fwing" || nObj.Followers != "fwers" ||
		nObj.Expire != "then" || nObj.ExpireTo != "other" ||
		nObj.Availability != "None" {
		t.Fatal("Database ObjectGet: equality failure:", nObj)
	}
	// Test slices
	if reflect.DeepEqual(nObj.Attachment, []activitypub.HathiID{"one", "two"}) != true ||
		reflect.DeepEqual(nObj.AttributedTo, []activitypub.HathiID{"three", "four"}) != true ||
		reflect.DeepEqual(nObj.Audience, []activitypub.HathiID{"five", "six"}) != true ||
		reflect.DeepEqual(nObj.Icon, []activitypub.HathiID{"seven", "eight"}) != true ||
		reflect.DeepEqual(nObj.Image, []activitypub.HathiID{"nine", "ten"}) != true ||
		reflect.DeepEqual(nObj.InReplyTo, []activitypub.HathiID{"eleven", "twelve"}) != true ||
		reflect.DeepEqual(nObj.Location, []activitypub.HathiID{"thirteen", "fourteen"}) != true ||
		reflect.DeepEqual(nObj.Tag, []activitypub.HathiID{"fifteen", "sixteen"}) != true ||
		reflect.DeepEqual(nObj.Url, []activitypub.HathiID{"seventeen", "eighteen"}) != true ||
		reflect.DeepEqual(nObj.To, []activitypub.HathiID{"nineteen", "twenty"}) != true ||
		reflect.DeepEqual(nObj.Bto, []activitypub.HathiID{"twenty-one", "twenty-two"}) != true ||
		reflect.DeepEqual(nObj.Cc, []activitypub.HathiID{"twenty-three", "twenty-four"}) != true ||
		reflect.DeepEqual(nObj.Bcc, []activitypub.HathiID{"twenty-five", "twenty-six"}) != true ||
		reflect.DeepEqual(nObj.Actor, []activitypub.HathiID{"actOne", "actTwo"}) != true ||
		reflect.DeepEqual(nObj.Target, []activitypub.HathiID{"tarOne", "tarTwo"}) != true ||
		reflect.DeepEqual(nObj.Instrument, []activitypub.HathiID{"insOne", "insTwo"}) != true ||
		reflect.DeepEqual(nObj.Items, []activitypub.HathiID{"1", "2", "3"}) != true {
		t.Fatal("Database ObjectGet: equality failure:", nObj)
	}
	// Test maps
	nameMap := map[string]string{"en":"John", "de":"Johan"}
	contentMap := map[string]string{"en":"No", "de":"Nein"}
	summaryMap := map[string]string{"en":"ding", "de":"dong"}
	if reflect.DeepEqual(nObj.Name, nameMap) != true ||
		reflect.DeepEqual(nObj.Content, contentMap) != true ||
		reflect.DeepEqual(nObj.Summary, summaryMap) != true {
		t.Fatal("Database ObjectGet: equality failure:", nObj)
	}
}

func TestDatabaseGetObjectCore(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Set expectations
	objRows := sqlmock.NewRows([]string{"TYPE", "ID", "SHADOW"}).
		AddRow("Test", "foo", "knows")
	mock.ExpectBegin()
	mock.ExpectQuery(`select TYPE, ID, SHADOW from OBJECTS`).
		WithArgs("foo").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	obj, err := db.GetObjectCore(nil, "foo")
	if err != nil {
		t.Fatal("Database GetBasicObjectData:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database GetBasicObjectData: unfulfilled expectations:", err)
	}
	if obj.Type != "Test" || obj.Id != "foo" || obj.Shadow != "knows" {
		t.Fatal("Database GetBasicObjectData: unexpected values:", obj)
	}
}

func TestDatabaseDeleteObject(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Set expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from OBJECTS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("delete from REFS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("delete from LANGMAP").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("delete from ITEMS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectCommit()

	err = db.DeleteObject(nil, "foo")
	if err != nil {
		t.Fatal("Database DeleteObject faliure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database DeleteObject unfulfilled expectations:", err)
	}
}
