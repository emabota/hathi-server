package dbase

import (
	//"fmt"
	//"log"
	"database/sql"
	"sort"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/hathi-social/hathi-server/activitypub"
)

func (h *HathiDB) SetObjectPrivileges(tx *sql.Tx, objID activitypub.HathiID, actors map[activitypub.HathiID][]string) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Sort our actors. If they aren't sorted we can't reliably test
	sortedActors := make([]string, len(actors))
	i := 0
	for actorID, _ := range actors {
		sortedActors[i] = string(actorID)
		i++
	}
	sort.Strings(sortedActors)
	// Clear old owners for this object
	_, err = tx.Exec("delete from PRIVILEGE where ID = $1;", objID)
	if err != nil {
		return err
	}
	// Set new data
	query := "insert into PRIVILEGE (ID, ACTOR, BITNAME) values ($1, $2, $3);"
	for _, strActorID := range sortedActors {
		actorID := activitypub.HathiID(strActorID)
		for _, priv := range actors[actorID] {
			_, err = tx.Exec(query, objID, actorID, priv)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (h *HathiDB) GetObjectPrivileges(tx *sql.Tx, objID activitypub.HathiID) (owners map[activitypub.HathiID][]string, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	owners = make(map[activitypub.HathiID][]string)
	ownerTable, err := h.DB.Query(`select ACTOR, BITNAME from PRIVILEGE where ID = $1`, objID)
	if err != nil {
		return nil, err
	}
	defer ownerTable.Close()
	var actor activitypub.HathiID
	var bitname string
	for ownerTable.Next() {
		err = ownerTable.Scan(&actor, &bitname)
		if err != nil {
			return nil, err
		}
		if _, ok := owners[actor]; ok == false {
			owners[actor] = make([]string, 0)
		}
		owners[actor] = append(owners[actor], bitname)
	}
	return owners, nil
}

func (h *HathiDB) SetObjActPrivileges(tx *sql.Tx, objID activitypub.HathiID, actorID activitypub.HathiID, privs []string) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Clear old data
	_, err = tx.Exec("delete from PRIVILEGE where ID = $1 and ACTOR = $2",
		objID, actorID)
	if err != nil {
		return err
	}
	// Set new data
	query := "insert into PRIVILEGE (ID, ACTOR, BITNAME) values ($1, $2, $3);"
	for _, priv := range privs {
		_, err = tx.Exec(query, objID, actorID, priv)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *HathiDB) GetObjActPrivileges(tx *sql.Tx, objID activitypub.HathiID, actorID activitypub.HathiID) (privs []string, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	privs = make([]string, 0)
	actorTable, err := h.DB.Query(`select BITNAME from PRIVILEGE where ID = $1 and ACTOR = $2;`, objID, actorID)
	if err != nil {
		return nil, err
	}
	defer actorTable.Close()
	var bitname string
	for actorTable.Next() {
		err = actorTable.Scan(&bitname)
		if err != nil {
			return nil, err
		}
		privs = append(privs, bitname)
	}
	return privs, nil
}

func (h *HathiDB) CheckPrivilege(tx *sql.Tx, objID activitypub.HathiID, actorID activitypub.HathiID, bitname string) (set bool, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return false, err
		}
		defer h.EndTransaction(tx, &err)
	}
	dataTable, err := h.DB.Query(`select * from PRIVILEGE where ID = $1 and ACTOR = $2 and BITNAME = $3;`, objID, actorID, bitname)
	if err != nil {
		return false, err
	}
	defer dataTable.Close()
	if dataTable.Next() == true {
		return true, nil
	} else {
		if err = dataTable.Err(); err != nil {
			return false, err
		} else {
			return false, nil
		}
	}
}

func (h *HathiDB) SetPrivilege(tx *sql.Tx, objID activitypub.HathiID, actorID activitypub.HathiID, bitname string, value bool) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	_, err = tx.Exec("delete from PRIVILEGE where ID = $1 and ACTOR = $2 and BITNAME = $3;", objID, actorID, bitname)
	if err != nil {
		return err
	}
	if value == true {
		_, err = tx.Exec("insert into PRIVILEGE (ID, ACTOR, BITNAME) values ($1, $2, $3);", objID, actorID, bitname)
		if err != nil {
			return err
		}
	}
	return nil
}
