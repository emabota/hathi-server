package dbase

import (
	//"fmt"
	"reflect"
	"testing"

	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestDatabaseItems(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// ======================================================
	// Test getI
	// ======================================================
	itemRows := sqlmock.NewRows([]string{"ITEMID"}).
		AddRow("two")
	mock.ExpectBegin()
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo", 1).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	entry, err := db.ItemsGetI(nil, "foo", 1)
	if err != nil {
		t.Fatal("Database CollectionGetI failed:", err)
	}
	if entry != "two" {
		t.Fatal("Database CollectionGetI equality failed:", entry)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database CollectionGetI: unfulfilled expectations:", err)
	}

	// ======================================================
	// Test getRange
	// ======================================================
	// Bounded upwards
	itemRows = sqlmock.NewRows([]string{"ITEMID"}).
		AddRow("two").
		AddRow("three")
	mock.ExpectBegin()
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo", 1, 3).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	entries, err := db.ItemsGetRange(nil, "foo", 1, 2)
	if err != nil {
		t.Fatal("Database CollectionGetRange upwards failed:", err)
	}
	items := []activitypub.HathiID{"two", "three"}
	if reflect.DeepEqual(entries, items) != true {
		t.Fatal("Database CollectionGetRange upwards equality failed:", entries)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionGetRange upwards: unfulfilled expectations:",
			err)
	}

	// Bounded downwards
	itemRows = sqlmock.NewRows([]string{"ITEMID"}).
		AddRow("one").
		AddRow("two")
	mock.ExpectBegin()
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo", -1, 2).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	entries, err = db.ItemsGetRange(nil, "foo", 1, -2)
	if err != nil {
		t.Fatal("Database CollectionGetRange downwards failed:", err)
	}
	items = []activitypub.HathiID{"one", "two"}
	if reflect.DeepEqual(entries, items) != true {
		t.Fatal("Database CollectionGetRange downwards equality failed:",
			entries)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionGetRange downwards: unfulfilled expectations:",
			err)
	}

	// Unbounded
	itemRows = sqlmock.NewRows([]string{"ITEMID"}).
		AddRow("two").
		AddRow("three").
		AddRow("four")
	mock.ExpectBegin()
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo", 1).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	entries, err = db.ItemsGetRange(nil, "foo", 1, 0)
	if err != nil {
		t.Fatal("Database CollectionGetRange unbounded failed:", err)
	}
	items = []activitypub.HathiID{"two", "three", "four"}
	if reflect.DeepEqual(entries, items) != true {
		t.Fatal("Database CollectionGetRange unbounded equality failed:",
			entries)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionGetRange unbounded: unfulfilled expectations:",
			err)
	}

	// ======================================================
	// Test append
	// ======================================================
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"MAXID"}).
		AddRow(1)
	mock.ExpectQuery("select max\\(ENTRYINDEX\\) as MAXID from ITEMS").
		WithArgs("foo").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into ITEMS").
		WithArgs("foo", 2, "item").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ItemsAppend(nil, "foo", "item")
	if err != nil {
		t.Fatal("Database CollectionAppend failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal( "Database CollectionAppend: unfulfilled expectations:", err)
	}

	// ======================================================
	// Test addI
	// ======================================================
	// Have to move an item
	itemRows = sqlmock.NewRows([]string{"ITEMID"}).
		AddRow("blah")
	mock.ExpectBegin()
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo", 4).
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"ID", "ENTRYINDEX", "ITEMID"}).
		AddRow("foo", 5, "block1").
		AddRow("foo", 4, "block0")
	mock.ExpectQuery("select \\* from ITEMS").
		WithArgs("foo", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("update ITEMS").
		WithArgs(6, "foo", 5).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("update ITEMS").
		WithArgs(5, "foo", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("insert into ITEMS").
		WithArgs("foo", 4, "neo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ItemsAddI(nil, "foo", 4, "neo")
	if err != nil {
		t.Fatal("Database CollectionAddI moving failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionAddI moving: unfulfilled expectations:",
			err)
	}

	// Don't have to move an item
	itemRows = sqlmock.NewRows([]string{"ITEMID"})
	mock.ExpectBegin()
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into ITEMS").
		WithArgs("foo", 4, "neo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ItemsAddI(nil, "foo", 4, "neo")
	if err != nil {
		t.Fatal("Database CollectionAddI non-moving failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionAddI non-moving: unfulfilled expectations:",
			err)
	}

	// ======================================================
	// Test rm
	// ======================================================
	// rm not at end
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"ITEMID"}).
		AddRow(4)
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from ITEMS").
		WithArgs("foo", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"ID", "ENTRYINDEX", "ITEMID"}).
		AddRow("foo", 5, "mover")
	mock.ExpectQuery("select \\* from ITEMS").
		WithArgs("foo", 5).
		WillReturnRows(itemRows)
	mock.ExpectExec("update ITEMS").
		WithArgs(4, "foo", 5).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ItemsRM(nil, "foo", 4)
	if err != nil {
		t.Fatal("Database CollectionRM failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database CollectionRM moving: unfulfilled expectations:", err)
	}

	// rm at end
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"ITEMID"}).
		AddRow(4)
	mock.ExpectQuery("select ITEMID from ITEMS").
		WithArgs("foo", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from ITEMS").
		WithArgs("foo", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"ID", "ENTRYINDEX", "ITEMID"})
	mock.ExpectQuery("select \\* from ITEMS").
		WithArgs("foo", 5).
		WillReturnRows(itemRows)
	mock.ExpectCommit()
	err = db.ItemsRM(nil, "foo", 4)
	if err != nil {
		t.Fatal("Database CollectionRM failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database CollectionRM non-moving: unfulfilled expectations:",
			err)
	}

	// ======================================================
	// Test edit
	// ======================================================
	mock.ExpectBegin()
	mock.ExpectExec("update ITEMS").
		WithArgs("bar", "foo", 1).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ItemsEdit(nil, "foo", 1, "bar")
	if err != nil {
		t.Fatal("Database CollectionEdit failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database CollectionEdit: unfulfilled expectations:", err)
	}

	// ======================================================
	// Test maxIndex
	// ======================================================
	// Empty
	itemRows = sqlmock.NewRows([]string{"MAXID"})
	mock.ExpectBegin()
	mock.ExpectQuery("select max\\(ENTRYINDEX\\) as MAXID from ITEMS").
		WithArgs("foo").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	highIndex, err := db.ItemsMaxIndex(nil, "foo")
	if err != nil {
		t.Fatal("Database CollectionMaxIndex failed:", err)
	}
	if highIndex != -1 {
		t.Fatal("Database CollectionMaxIndex index error:", highIndex)
	}

	// Filled
	itemRows = sqlmock.NewRows([]string{"MAXID"}).
		AddRow(4)
	mock.ExpectBegin()
	mock.ExpectQuery("select max\\(ENTRYINDEX\\) as MAXID from ITEMS").
		WithArgs("foo").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	highIndex, err = db.ItemsMaxIndex(nil, "foo")
	if err != nil {
		t.Fatal("Database CollectionMaxIndex failed:", err)
	}
	if highIndex != 4 {
		t.Fatal("Database CollectionMaxIndex index error:", highIndex)
	}
}
