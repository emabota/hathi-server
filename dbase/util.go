package dbase

import (
	"database/sql"
	"sort"

	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
)

type boxEntry struct {
	CollectionID string
	EntryID      string
	Index        int
}

func (b *boxEntry) isEqual(other *boxEntry) bool {
	if b.Index != other.Index {
		return false
	} else if b.CollectionID != other.CollectionID {
		return false
	} else if b.EntryID != other.EntryID {
		return false
	}
	return true
}

// The caller is responsible for closing the rows
func convertBoxRowsToSlice(rows *sql.Rows) (entries []*boxEntry, err error) {
	for rows.Next() {
		entry := new(boxEntry)
		err = rows.Scan(&entry.CollectionID, &entry.Index, &entry.EntryID)
		if err != nil {
			return nil, err
		}
		entries = append(entries, entry)
	}
	return entries, nil
}

func (h *HathiDB) coreReferenceSet(tx *sql.Tx, objID activitypub.HathiID, typeLabel string, s *[]activitypub.HathiID) (err error) {
	setRef := `insert into REFS (ID, TYPE, REFID) values ($1, $2, $3);`
	// Loop through each list on each reference type field
	for _, refID := range *s {
		_, err = tx.Exec(setRef, objID, typeLabel, refID)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *HathiDB) coreLangmapSet(tx *sql.Tx, objID activitypub.HathiID, typeLabel string, langmap *map[string]string) (err error) {
	setLang := `insert into LANGMAP (ID, TYPE, LANG, DATA) values ($1, $2, $3, $4);`
	sortedLangs := make([]string, len(*langmap))
	i := 0
	for lang, _ := range *langmap {
		sortedLangs[i] = lang
		i++
	}
	sort.Strings(sortedLangs)
	for _, lang := range sortedLangs {
		_, err = tx.Exec(setLang, objID, typeLabel, lang, (*langmap)[lang])
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *HathiDB) coreReferenceGet(tx *sql.Tx, objID activitypub.HathiID, typeLabel string) (ref []activitypub.HathiID, err error) {
	var s string
	refTable, err := tx.Query(
		`select REFID from REFS where ID = $1 and TYPE = $2`,
		objID, typeLabel)
	if err != nil {
		return nil, err
	}
	defer refTable.Close()
	for refTable.Next() {
		err = refTable.Scan(&s)
		if err != nil {
			return nil, err
		}
		ref = append(ref, activitypub.HathiID(s))
	}
	refTable.Close()
	return ref, nil
}

func (h *HathiDB) coreLangmapGet(tx *sql.Tx, objID activitypub.HathiID, typeLabel string) (langmap map[string]string, err error) {
	langmap = make(map[string]string)
	var lang, data string
	langTable, err := tx.Query(
		`select LANG, DATA from LANGMAP where ID = $1 and TYPE = $2`,
		objID, typeLabel)
	if err != nil {
		return nil, err
	}
	defer langTable.Close()
	for langTable.Next() {
		err = langTable.Scan(&lang, &data)
		if err != nil {
			return nil, err
		}
		langmap[lang] = data
	}
	langTable.Close()
	return langmap, nil
}

// Move everything from top through snipIndex up or down
func (h *HathiDB) shiftEntries(tx *sql.Tx, tableID activitypub.HathiID, snipIndex int, movement int) (err error) {
	setExec := "update ITEMS set ENTRYINDEX = $1 where ID = $2 and ENTRYINDEX = $3"
	queryRoot := "select * from ITEMS where ID = $1 and ENTRYINDEX >= $2 order by ENTRYINDEX "
	var outbox *sql.Rows
	// Get the block of rows that we are modifying
	if movement > 0 { // Move up
		// Get list
		outbox, err = tx.Query(queryRoot+"desc", tableID, snipIndex)
		if err != nil {
			return err
		}
	} else if movement < 0 { // Move down
		outbox, err = tx.Query(queryRoot+"asc", tableID, snipIndex)
	} else { // movement == 0
		return nil // NOP, should this be an error?
	}
	defer outbox.Close()
	entries, err := convertBoxRowsToSlice(outbox)
	if err != nil {
		return err
	}
	// Have the rows in a convienent format, now move them around
	for _, item := range entries {
		newIndex := item.Index + movement
		_, err = tx.Exec(setExec, newIndex, item.CollectionID, item.Index)
		if err != nil { // Problem: the db is in an unknown state here
			return err
		}
	}
	return nil
}

func (h *HathiDB) getAllTableIDs(tx *sql.Tx, tableName string) (ids []activitypub.HathiID, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	objTable, err := tx.Query(`select ID from ` + tableName)
	defer objTable.Close()
	ids = make([]activitypub.HathiID, 0)
	var entry activitypub.HathiID
	for objTable.Next() {
		err = objTable.Scan(&entry)
		if err != nil {
			return nil, err
		}
		ids = append(ids, entry)
	}
	return ids, nil
}
