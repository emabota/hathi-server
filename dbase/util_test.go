package dbase

import (
	//"fmt"
	"reflect"
	"testing"

	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestDatabaseGetObjectIDs(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test GetAllObjectIDs
	// Set expectations
	objRows := sqlmock.NewRows([]string{"ID"}).
		AddRow("a").
		AddRow("b").
		AddRow("c")
	mock.ExpectBegin()
	mock.ExpectQuery(`select ID from FOO`).
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	ids, err := db.getAllTableIDs(nil, "FOO")
	if err != nil {
		t.Fatal("Database getAllTableIDs:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database getAllTableIDs: unfulfilled expectations:", err)
	}
	if reflect.DeepEqual([]activitypub.HathiID{"a", "b", "c"}, ids) == false {
		t.Fatal("Database getAllTableIDs: incorrect values:", ids)
	}
}
