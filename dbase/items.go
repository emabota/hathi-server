package dbase

import (
	//"fmt"
	"database/sql"
	"gitlab.com/hathi-social/hathi-server/activitypub"
)

func (h *HathiDB) ItemsGetI(tx *sql.Tx, tableID activitypub.HathiID, index int) (entry activitypub.HathiID, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return "", err
		}
		defer h.EndTransaction(tx, &err)
	}
	query := "select ITEMID from ITEMS where ID = $1 and ENTRYINDEX = $2"
	items, err := h.DB.Query(query, tableID, index)
	if err != nil {
		return "", err
	}
	defer items.Close()
	if items.Next() != true {
		return "", items.Err()
	}
	var tmp string
	err = items.Scan(&tmp)
	if err != nil {
		return "", err
	}
	entry = activitypub.HathiID(tmp)
	return entry, nil
}

func (h *HathiDB) ItemsGetRange(tx *sql.Tx, tableID activitypub.HathiID, start int, length int) (entries []activitypub.HathiID, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	var outbox *sql.Rows
	if length > 0 { // Bounded range, upwards
		query := "select ITEMID from ITEMS where ID = $1 and ENTRYINDEX >= $2 and ENTRYINDEX < $3 order by ENTRYINDEX asc"
		outbox, err = h.DB.Query(query, tableID, start, start+length)
	} else if length < 0 { // Bounded range, downwards
		query := "select ITEMID from ITEMS where ID = $1 and ENTRYINDEX >= $2 and ENTRYINDEX < $3 order by ENTRYINDEX asc"
		outbox, err = h.DB.Query(query, tableID, start+length, start+1)
	} else { // Unbounded range
		query := "select ITEMID from ITEMS where ID = $1 and ENTRYINDEX >= $2 order by ENTRYINDEX asc"
		outbox, err = h.DB.Query(query, tableID, start)
	}
	if err != nil {
		return nil, err
	}
	defer outbox.Close()
	var entry string
	for outbox.Next() {
		err = outbox.Scan(&entry)
		if err != nil {
			return nil, err
		}
		entries = append(entries, activitypub.HathiID(entry))
	}
	return entries, nil
}

func (h *HathiDB) ItemsAppend(tx *sql.Tx, tableID activitypub.HathiID, entry activitypub.HathiID) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	setExec := "insert into ITEMS (ID, ENTRYINDEX, ITEMID) values ($1, $2, $3);"

	index, err := h.ItemsMaxIndex(tx, tableID)
	if err != nil {
		return err
	}
	index++
	_, err = tx.Exec(setExec, tableID, index, entry)
	if err != nil {
		return err
	}
	return nil
}

func (h *HathiDB) ItemsAddI(tx *sql.Tx, tableID activitypub.HathiID, index int, entry activitypub.HathiID) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Check that row doesn't exist
	exists, err := h.ItemsGetI(tx, tableID, index)
	if err != nil {
		return err
	}
	if exists != "" { // There is something at the index
		// Move everything out of the way
		err = h.shiftEntries(tx, tableID, index, 1)
		if err != nil {
			return err
		}
	}
	// Add row
	query := "insert into ITEMS (ID, ENTRYINDEX, ITEMID) values ($1, $2, $3);"
	_, err = tx.Exec(query, tableID, index, entry)
	return err
}

func (h *HathiDB) ItemsRM(tx *sql.Tx, tableID activitypub.HathiID, index int) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	query := "delete from ITEMS where ID = $1 and ENTRYINDEX = $2"
	// Check that it exists
	entry, err := h.ItemsGetI(tx, tableID, index)
	if err != nil {
		return err
	}
	if entry == "" { // Nothing at that index
		return nil // Should this be an error?
	}
	// Delete the entry
	_, err = tx.Exec(query, tableID, index)
	if err != nil {
		return err
	}
	// Move everything down
	err = h.shiftEntries(tx, tableID, index+1, -1)
	if err != nil {
		return err
	}
	return nil
}

func (h *HathiDB) ItemsEdit(tx *sql.Tx, tableID activitypub.HathiID, index int, entry activitypub.HathiID) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	query := "update ITEMS set ITEMID = $1 where ID = $2 and ENTRYINDEX = $3"
	_, err = tx.Exec(query, entry, tableID, index)
	if err != nil {
		return err
	}
	return nil
}

func (h *HathiDB) ItemsMaxIndex(tx *sql.Tx, tableID activitypub.HathiID) (index int, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return 0, err
		}
		defer h.EndTransaction(tx, &err)
	}
	query := "select max(ENTRYINDEX) as MAXID from ITEMS where ID = $1"
	// Get highest index
	rows, err := h.DB.Query(query, tableID)
	if err != nil {
		return 0, err
	}
	defer rows.Close()
	if rows.Next() != true { // Nothing in DB, start at 0
		index = -1
	} else {
		rows.Scan(&index)
	}
	return index, nil
}
