package dbase

import (
	//"fmt"
	//"log"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/hathi-social/hathi-server/activitypub"
)

var Logging bool

// The Go DB is wrapped so that other code is as isolated as possible from
// the details of what database type is being dealt with. There may be other
// data to track in future so we save on transition costs if that happens.
// The cost is that we have to implement methods like Close()
type HathiDB struct {
	DB *sql.DB
}

// The core tables for the basic ActivityPub Object
var objTables = []string{"OBJECTS", "REFS", "LANGMAP", "ITEMS"}

func OpenDatabase(dbtype string, filename string) (db *HathiDB, err error) {
	db = new(HathiDB)
	db.DB, err = sql.Open(dbtype, filename)
	if err != nil {
		return nil, err
	}
	return db, err
}

func (h *HathiDB) Initialize() (err error) {
	tx, err := h.DB.Begin()
	if err != nil {
		return err
	}

	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()

	_, err = h.DB.Exec(
		`create table OBJECTS (TYPE text, ID text, ATCONTEXT text, CONTEXT text, ENDTIME text, GENERATOR text, PREVIEW text, PUBLISHED text, REPLIES text, STARTTIME text, UPDATED text, MEDIATYPE text, DURATION text, FORMERTYPE test, DELETED text, SHADOW text, ACTOBJECT text, RESULT text, ORIGIN text, INBOX text, OUTBOX text, FOLLOWING text, FOLLOWERS text, EXPIRE text, EXPIRETO text, AVAILABILITY text);`)
	if err != nil {
		return err
	}
	// REFERENCES covers: attachment, attributedTo, audience, icon, image,
	//     inReplyTo, location, tag, to, bto, cc, bcc
	_, err = h.DB.Exec(`create table REFS (ID text, TYPE text, REFID text);`)
	if err != nil {
		return err
	}
	// LANGMAP covers: name, content, summary
	_, err = h.DB.Exec(`create table LANGMAP (ID text, TYPE text, LANG text, DATA text);`)
	if err != nil {
		return err
	}
	_, err = h.DB.Exec(`create table ITEMS (ID text, ENTRYINDEX int, ITEMID text);`)
	if err != nil {
		return err
	}
	_, err = h.DB.Exec(`create table PRIVILEGE (ID text, ACTOR text, BITNAME string);`)
	if err != nil {
		return err
	}

	return nil
}

func (h *HathiDB) Close() (err error) {
	err = h.DB.Close()
	if err != nil {
		return err
	}
	h.DB = nil
	return nil
}

func (h *HathiDB)BeginTransaction() (tx *sql.Tx, err error) {
	tx, err = h.DB.Begin()
	if err != nil {
		return nil, err
	}
	return tx, nil
}

func (h *HathiDB)EndTransaction(tx *sql.Tx, errIn *error) (err error) {
	if errIn == nil {
		err = tx.Commit()
	} else {
		switch *errIn {
		case nil:
			err = tx.Commit()
		default:
			err = tx.Rollback()
		}
	}
	return err
}

func (h *HathiDB)GetAllObjectIDs(tx *sql.Tx) (ids []activitypub.HathiID, err error) {
	return h.getAllTableIDs(tx, "OBJECTS")
}

func (h *HathiDB)GetAllPrivilegeIDs(tx *sql.Tx) (ids []activitypub.HathiID, err error) {
	return h.getAllTableIDs(tx, "PRIVILEGE")
}
