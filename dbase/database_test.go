package dbase

import (
	//"fmt"
	//"reflect"
	"testing"
	"errors"

	//"gitlab.com/hathi-social/hathi-server/activitypub"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestDatabaseCOC(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test the creation of each table
	mock.ExpectBegin()
	mock.ExpectExec(`create table OBJECTS`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table REFS`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table LANGMAP`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table ITEMS`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table PRIVILEGE`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	db.Initialize()

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database Initialize: unfulfilled expectations:", err)
	}

	// Test closing
	mock.ExpectClose()

	db.Close()

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database Close: unfulfilled expectations:", err)
	}
}

func TestBeginTransaction(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	mock.ExpectBegin()

	tx, err := db.BeginTransaction()

	if tx == nil || err != nil {
		t.Fatal("Database BeginTransaction error:", tx, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database BeginTransaction: unfulfilled expectations:", err)
	}
}

func TestEndTransaction(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test without error control
	mock.ExpectBegin()
	mock.ExpectCommit()
	tx, err := db.BeginTransaction()
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	err = db.EndTransaction(tx, nil)
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database EndTransaction: unfulfilled expectations:", err)
	}

	// Test with error control, success
	mock.ExpectBegin()
	mock.ExpectCommit()
	tx, err = db.BeginTransaction()
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	testError := error(nil)
	err = db.EndTransaction(tx, &testError)
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database EndTransaction: unfulfilled expectations:", err)
	}

	// Test with error control, rollback
	mock.ExpectBegin()
	mock.ExpectRollback()
	tx, err = db.BeginTransaction()
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	testError = errors.New("foo")
	err = db.EndTransaction(tx, &testError)
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database EndTransaction: unfulfilled expectations:", err)
	}
}
