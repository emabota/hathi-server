package dbase

import (
	"database/sql"
	//"sort"

	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
)

func (h *HathiDB) SetObject(tx *sql.Tx, o *activitypub.Object) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	objID := o.GetID() // So we aren't calling this 50 times
	for _, tableName := range objTables {
		_, err = tx.Exec("delete from "+tableName+" where ID = $1;", objID)
		if err != nil {
			return err
		}
	}

	// Fill OBJECTS table
	setObj := `insert into OBJECTS (TYPE, ID, CONTEXT, ATCONTEXT, ENDTIME, GENERATOR, PREVIEW, PUBLISHED, REPLIES, STARTTIME, UPDATED, MEDIATYPE, DURATION, FORMERTYPE, DELETED, SHADOW, ACTOBJECT, RESULT, ORIGIN, INBOX, OUTBOX, FOLLOWING, FOLLOWERS, EXPIRE, EXPIRETO, AVAILABILITY) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26);`
	_, err = tx.Exec(setObj, o.Type, objID, o.Context, o.AtContext, o.EndTime,
		o.Generator, o.Preview, o.Published, o.Replies, o.StartTime,
		o.Updated, o.MediaType, o.Duration, o.FormerType, o.Deleted, o.Shadow,
		o.ActObject, o.Result, o.Origin, o.Inbox, o.Outbox, o.Following,
		o.Followers, o.Expire, o.ExpireTo, o.Availability)
	if err != nil {
		return err
	}
	// Fill REFS table
	refTable := [][]interface{}{
		{"attachment", &o.Attachment},
		{"attributedTo", &o.AttributedTo},
		{"audience", &o.Audience},
		{"icon", &o.Icon},
		{"image", &o.Image},
		{"inReplyTo", &o.InReplyTo},
		{"location", &o.Location},
		{"tag", &o.Tag},
		{"to", &o.To},
		{"bto", &o.Bto},
		{"cc", &o.Cc},
		{"bcc", &o.Bcc},
		{"url", &o.Url},
		{"actor", &o.Actor},
		{"target", &o.Target},
		{"instrument", &o.Instrument},
	}
	for _, data := range refTable {
		err = h.coreReferenceSet(
			tx, objID, data[0].(string), data[1].(*[]activitypub.HathiID))
		if err != nil {
			return err
		}
	}
	// Fill LANGMAP table
	langTable := [][]interface{}{
		{"name", &o.Name},
		{"content", &o.Content},
		{"summary", &o.Summary},
	}
	for _, langmap := range langTable {
		err = h.coreLangmapSet(
			tx, objID, langmap[0].(string), langmap[1].(*map[string]string))
		if err != nil {
			return err
		}
	}
	// Set Items
	query := "insert into ITEMS (ID, ENTRYINDEX, ITEMID) values ($1, $2, $3)"
	for index, entry := range o.Items {
		_, err = tx.Exec(query, o.Id, index, entry)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *HathiDB) GetObject(tx *sql.Tx, objID activitypub.HathiID) (o *activitypub.Object, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	o = new(activitypub.Object)
	// OBJECTS table
	objTable, err := tx.Query(`select TYPE, ID, CONTEXT, ATCONTEXT, ENDTIME, GENERATOR, PREVIEW, PUBLISHED, REPLIES, STARTTIME, UPDATED, MEDIATYPE, DURATION, FORMERTYPE, DELETED, SHADOW, ACTOBJECT, RESULT, ORIGIN, INBOX, OUTBOX, FOLLOWING, FOLLOWERS, EXPIRE, EXPIRETO, AVAILABILITY from OBJECTS where ID = $1`, objID)
	if err != nil {
		return nil, err
	}
	defer objTable.Close()
	if objTable.Next() != true {
		return nil, objTable.Err()
	}
	err = objTable.Scan(&o.Type, &o.Id, &o.Context, &o.AtContext, &o.EndTime,
		&o.Generator, &o.Preview, &o.Published, &o.Replies, &o.StartTime,
		&o.Updated, &o.MediaType, &o.Duration, &o.FormerType, &o.Deleted,
		&o.Shadow, &o.ActObject, &o.Result, &o.Origin, &o.Inbox, &o.Outbox,
		&o.Following, &o.Followers, &o.Expire, &o.ExpireTo, &o.Availability)
	if err != nil {
		return nil, err
	}
	objTable.Close()
	// REFS table
	refTypes := [][]interface{}{
		{"attachment", &o.Attachment},
		{"attributedTo", &o.AttributedTo},
		{"audience", &o.Audience},
		{"icon", &o.Icon},
		{"image", &o.Image},
		{"inReplyTo", &o.InReplyTo},
		{"location", &o.Location},
		{"tag", &o.Tag},
		{"to", &o.To},
		{"bto", &o.Bto},
		{"cc", &o.Cc},
		{"bcc", &o.Bcc},
		{"url", &o.Url},
		{"actor", &o.Actor},
		{"target", &o.Target},
		{"instrument", &o.Instrument},
	}
	for _, refData := range refTypes {
		ref, err := h.coreReferenceGet(tx, objID, refData[0].(string))
		if err != nil {
			return nil, err
		}
		*refData[1].(*[]activitypub.HathiID) = ref
	}
	// LANGMAP table
	langTypes := [][]interface{}{
		{"name", &o.Name},
		{"content", &o.Content},
		{"summary", &o.Summary},
	}
	for _, langData := range langTypes {
		lang, err := h.coreLangmapGet(tx, objID, langData[0].(string))
		if err != nil {
			return nil, err
		}
		*langData[1].(*map[string]string) = lang
	}
	// Items list
	query := "select ITEMID from ITEMS where ID = $1 order by ENTRYINDEX asc"
	items, err := tx.Query(query, objID)
	if err != nil {
		return nil, err
	}
	defer items.Close()
	// Slurp up the data
	var entry string
	for items.Next() {
		err = items.Scan(&entry)
		if err != nil {
			return nil, err
		}
		o.Items = append(o.Items, activitypub.HathiID(entry))
	}
	return o, nil
}

func (h *HathiDB) GetObjectCore(tx *sql.Tx, objID activitypub.HathiID) (obj *activitypub.Object, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	obj = new(activitypub.Object)
	objTable, err := tx.Query(`select TYPE, ID, SHADOW from OBJECTS where ID = $1`, objID)
	if err != nil {
		return nil, err
	}
	defer objTable.Close()
	if objTable.Next() != true {
		return nil, objTable.Err()
	}
	err = objTable.Scan(&obj.Type, &obj.Id, &obj.Shadow)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func (h *HathiDB) DeleteObject(tx *sql.Tx, objID activitypub.HathiID) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	for _, tableName := range objTables {
		_, err = tx.Exec("delete from "+tableName+" where ID = $1;", objID)
		if err != nil {
			return err
		}
	}
	return nil
}
