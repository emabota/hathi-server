# Hathi Server

## Developing

**Dependencies**

go-sqlite3
gin-gonic

**1. Install the Go language**
Follow the official instructions at [GoLang](https://golang.org/doc/install) or
```
sudo apt-get install golang
```

**2. Clone from repository**
```
go get gitlab.com/hathi-social/hathi-server
```

**3. Move into hathi-server directory**
```
cd ~/go/src/gitlab.com/hathi-social/hathi-server
```

**4. Run Server**
```
go run server.go -port <port> -host <host>
```
Port and Host are optional, if ommited the server will default to running on
localhost:8080

## Testing

The tests are currently isolated to specific modules, to run them the user must
cd into the relevant directory and run go test from there

Modules with tests: handlers, dbase

```
go test
```

## Dumping / Restoring the database to a file

hathi-server has the -dump <file> option, which exports the database in JSON
format to the specified file and exits. Paired with the -restore <file> option
(NOT YET IMPLEMENTED) allows the server admin to update their version of
hathi-server without potential database incompatabilities.
