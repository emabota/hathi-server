package main

import (
	"fmt"
	"log"
	"flag"
	"strconv"
	"bytes"
	"io/ioutil"
	"encoding/json"

	"github.com/gin-gonic/gin"
	"gitlab.com/hathi-social/hathi-server/hathi"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gitlab.com/hathi-social/hathi-server/handlers"
	"gitlab.com/hathi-social/hathi-server/dbase"
)

func main() {
	var hostname string
	var port int
	var logging bool
	var dumping, restoring string

	flag.StringVar(&hostname, "host", "localhost", "Hostname of the server")
	flag.IntVar(&port, "port", 8080, "Port to listen on")
	flag.BoolVar(&logging, "log", false, "Turns on error logging")
	flag.StringVar(&dumping, "dump", "",
		"Dumps the database to the specified JSON file")
	flag.StringVar(&restoring, "restore", "",
		"Restores the database from the specified JSON file")

	flag.Parse()

	hathi.Logging = logging
	activitypub.Logging = logging
	handlers.Logging = logging
	dbase.Logging = logging

	hathi.InitEngine(hostname, strconv.Itoa(port), "hathi.db")

	if dumping != "" {
		buf, err := DoDump()
		if err != nil {
			log.Fatal(err)
		}
		bees := buf.Bytes()
		err = ioutil.WriteFile(dumping, bees, 0644)
		if err != nil {
			log.Fatal(err)
		}
		return
	} else if restoring != "" {
		data, err := ioutil.ReadFile(restoring)
		if err != nil {
			log.Fatal(err)
		}
		err = DoRestore(data)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Database loaded from", restoring)
		return
	}

	// Spawn our remote propagation handler
	go hathi.Engine.DoRemotePropagation()

	router := gin.Default()

	router.GET("/*w", handlers.GetHandler)
	router.POST("/:actorname/inbox", handlers.InboxPostHandler)
	router.POST("/:actorname/outbox", handlers.OutboxPostHandler)

	log.Fatal(router.Run(fmt.Sprintf(":%d", port)))
}

func DoDump() (buf *bytes.Buffer, err error) {
	dump, err := hathi.Engine.DumpDB()
	if err != nil {
		return nil, err
	}
	// Prepare Objects
	objdata := make([]map[string]interface{}, 0)
	for _, obj := range dump.Objects {
		mobj := obj.Mapify()
		objdata = append(objdata, mobj)
	}
	data := make(map[string]interface{})
	data["objects"] = objdata
	data["privileges"] = dump.Privileges
	dumpInfo := make(map[string]interface{})
	dumpInfo["major_version"] = dump.DumpInfo.MajorVersion
	dumpInfo["minor_version"] = dump.DumpInfo.MinorVersion
	dumpInfo["bugfix_version"] = dump.DumpInfo.BugFixVersion
	dumpInfo["dump_time"] = dump.DumpInfo.DumpTime
	data["dump_info"] = dumpInfo
	// Marshal
	unpretty, err := json.Marshal(data)
	// Create pretty form
	buf = bytes.NewBuffer(nil)
	json.Indent(buf, unpretty, "", "  ")
	return buf, nil
}

func DoRestore(data []byte) (err error) {
	var tmp interface{}
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return err
	}
	jsonData := tmp.(map[string]interface{})
	dump := hathi.DumpData{}
	// Parse Objects
	dump.Objects = make([]*activitypub.Object, 0)
	// Loop through..... crap what about sub-objects?
	for _, tmp := range jsonData["objects"].([]interface{}) {
		obj := tmp.(map[string]interface{})
		decoded, _, err := activitypub.DecodeObjectMap(obj, "")
		// We are ignoring the sub-objects for now. A dump won't have them.
		// TODO: The Great Refactor will change this
		if err != nil {
			return err
		}
		dump.Objects = append(dump.Objects, decoded)
	}
	// Parse Privileges
	dump.Privileges = make(map[activitypub.HathiID]map[activitypub.HathiID][]string, 0)
	// Loop through each object
	for oID, privs := range jsonData["privileges"].(map[string]interface{}) {
		objID := activitypub.HathiID(oID)
		privSet := make(map[activitypub.HathiID][]string, 0)
		// Loop through each actor associated with that object
		for aID, tmp := range privs.(map[string]interface{}) {
			actorID := activitypub.HathiID(aID)
			interList := tmp.([]interface{})
			bitList := make([]string, 0)
			// Have to loop through the bits and make them strings
			for _, bit := range interList {
				bitList = append(bitList, bit.(string))
			}
			privSet[actorID] = bitList
		}
		dump.Privileges[objID] = privSet
	}
	err = hathi.Engine.LoadDB(&dump)
	return nil
}
