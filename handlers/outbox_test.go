package handlers

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/hathi-social/hathi-server/activitypub"
)

func TestPostNoteToOutbox(t *testing.T) {
	alyssa := activitypub.NewActor("alyssa")

	activitypub.Engine = &activitypub.Core{}
	activitypub.Engine.LocalURL = "localhost.localdomain"
	activitypub.Engine.Actors = append(activitypub.Engine.Actors, alyssa)

	ch := make(chan activitypub.Activity)
	ActivitiesChan = ch
	go func() {
		activity := <-ch
		if activity.GetType() != "Note" {
			t.Error("Activity type is not 'Note': ", activity.GetType())
		}
	}()

	router := gin.New()
	router.POST("/:username/outbox/", OutboxPostHandler)

	var jsonReq = []byte(`{
		"@context": "https://www.w3.org/ns/activitystreams",
		"type": "Note",
		"to": ["https://chatty.example/ben/"],
		"attributedTo": "https://social.example/alyssa/",
		"content": "Say, did you finish reading that book I lent you?"
	}`)

	req, err := http.NewRequest("POST", "/alyssa/outbox/", bytes.NewBuffer(jsonReq))
	req.Header.Set("Content-Type", "application/json")
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	jsonResp := strings.TrimSpace(resp.Body.String())

	if err != nil {
		t.Fatal("Error is not nil: ", err)
	}

	if resp.Code != http.StatusCreated {
		t.Error("Error code is not status Created: ", resp.Code)
	}

	if resp.HeaderMap.Get("Content-Type") != "application/json; charset=utf-8" {
		t.Error("Content-Type is not the expected: ", resp.HeaderMap.Get("Content-Type"))
	}

	if resp.HeaderMap.Get("Location") != "https://localhost.localdomain/alyssa/posts/1" {
		t.Error("Location Header is not the expected: ", resp.HeaderMap.Get("Location"))
	}

	if jsonResp != "null" {
		t.Error("Response json is not the expected: ", jsonResp)
	}
}

func TestPostBadJSONToOutbox(t *testing.T) {
	router := gin.New()
	router.POST("/:username/outbox/", OutboxPostHandler)

	var jsonReq = []byte(`{"invalid": "content"}`)

	req, err := http.NewRequest("POST", "/actor/outbox/", bytes.NewBuffer(jsonReq))
	if err != nil {
		t.Fatal("Error is not nil: ", err)
	}

	req.Header.Set("Content-Type", "application/json")
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	jsonResp := strings.TrimSpace(resp.Body.String())

	if resp.Code != http.StatusBadRequest {
		t.Error("Error code is not status Bad Request: ", resp.Code)
	}

	if resp.HeaderMap.Get("Content-Type") != "application/json; charset=utf-8" {
		t.Error("Content-Type is not the expected: ", resp.HeaderMap.Get("Content-Type"))
	}

	if jsonResp != "{\"error\":\"Unrecognized ActivityPub Type\"}" {
		t.Error("Response json is not the expected: ", jsonResp)
	}
}
