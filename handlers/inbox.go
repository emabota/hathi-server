package handlers

import (
	//"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gitlab.com/hathi-social/hathi-server/hathi"
)

// OnboxPostHandler handles POST requests on Inbox endpoint for a user
func InboxPostHandler(c *gin.Context) {
	// Check server certs.
	// Server to server authentication not yet implemented

	// We get the target actor's localid from the URL of the POST
	actorID := hathi.Engine.MakeIDFromLocalID(c.Param("actorname"))

	if Logging {
		log.Println("Inbox POST", actorID)
	}

	// Extract the data from the packet
	data, err := c.GetRawData()
	if err != nil {
		if Logging {
			log.Println("Inbox POST body retrevial error;", err)
		}
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Invalid ActivityPub Stream"})
		return
	}

	// Turn JSON strings into actual objects
	obj, subObjects, err := activitypub.DecodeJSON(data)
	if err != nil {
		if Logging {
			log.Println("Inbox POST JSON decode error;", err)
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid JSON"})
		return
	}

	status, extErr, err := hathi.Engine.PostInbox(true, actorID, obj,
		subObjects)
	if err != nil {
		if Logging {
			log.Println("Error:", err)
		}
	}
	if extErr == "" {
		c.Status(status)
	} else {
		c.String(status, extErr)
	}
	return
}
