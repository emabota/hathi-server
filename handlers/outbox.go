package handlers

import (
	//"fmt"
	"net/http"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gitlab.com/hathi-social/hathi-server/hathi"
)

// OutboxPostHandler handles POST request on Outbox endpoint for a user
func OutboxPostHandler(c *gin.Context) {
	// Check user, until security implemented assume authorized
	// We know the user from the authentication, *not* the json

	var state hathi.OutboxPosting
	// We get the target actor's localid from the URL of the POST
	tmp := c.Param("actorname")
	if Logging {
		log.Println("Actor name:", tmp)
	}
	state.ActorID = hathi.Engine.MakeIDFromLocalID(tmp)

	if Logging {
		log.Println("Outbox POST", state.ActorID)
	}

	// Extract the data from the packet
	data, err := c.GetRawData()
	if err != nil {
		if Logging {
			log.Println("Outbox POST body retrevial error;", err)
		}
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Invalid ActivityPub Stream"})
		return
	}

	// Turn JSON strings into actual objects
	var obj *activitypub.Object
	obj, state.Subs, err = activitypub.DecodeJSON(data)
	if (err != nil) || (obj == nil) {
		if Logging {
			log.Println("Outbox POST JSON decode error;", err)
			log.Println("Failed data:", string(data))
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid JSON"})
		return
	}
	
	// Check for bare objects and wrap then if necessary
	if !obj.IsActivity() {
		// It is an object, wrap it in a Create.
		// Only activities are allowed to be posted (per spec)
		state.Activity = *(makeCreateWrapper(state.ActorID, obj))
		state.Subs["^wrapCreate"] = obj
		state.Activity.ActObject = "^wrapCreate"
	} else {
		// The object is an activity, cast so it is easier to work with
		state.Activity = *(obj)
	}
	// At this point the object will always be an Activity of some sort
	
	// Discover whether this is an valid Actor, invalid Actor, or creating
	// an Actor.
	actor, _ := hathi.Engine.GetObject(state.ActorID)
	if (state.Activity.GetType() == "Create") &&
		state.Subs[state.Activity.ActObject].IsActor() {
		// Attempted Actor Creation
		if actor == nil {
			// Make sure the actor doesn't already exist
			status, extErr, loc, err := hathi.Engine.PostActor(&state)
			if err != nil {
				if Logging {
					log.Println("Error:", err)
				}
			}
			if extErr == "" {
				c.Status(status)
			} else {
				c.String(status, extErr)
			}
			if loc != "" {
				c.Header("Location", loc)
			}
		}
	} else {
		status, extErr, loc, err := hathi.Engine.PostOutboxActivity(&state)
		if err != nil {
			if Logging {
				log.Println("Error:", err)
			}
		}
		if extErr == "" {
			c.Status(status)
		} else {
			c.String(status, extErr)
		}
		if loc != "" {
			c.Header("Location", loc)
		}
	}
	return
}

func makeCreateWrapper(actorID activitypub.HathiID, object activitypub.ActivityPub) (cre *activitypub.Object) {
	cre = new(activitypub.Object)
	cre.Type = "Create"
	coreObj := object.GetCore().(*activitypub.Object)
	// Propagation and distribution information should be the same for both
	// the Create and the created Object
	cre.To = coreObj.To
	cre.Bto = coreObj.Bto
	cre.Cc = coreObj.Cc
	cre.Bcc = coreObj.Bcc
	cre.Audience = coreObj.Audience
	cre.Published = coreObj.Published
	cre.Origin = actorID
	return cre
}
