package handlers

import (
	//"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	//"gitlab.com/hathi-social/hathi-server/activitypub"
	"gitlab.com/hathi-social/hathi-server/hathi"
)

func GetHandler(c *gin.Context) {
	id := hathi.Engine.MakeIDFromLocalID(c.Request.URL.String())

	if Logging {
		log.Println("GET;", id)
	}

	obj, err := hathi.Engine.GetObject(id)
	// Check authorization
	if (err != nil) || (obj == nil) {
		if Logging {
			log.Println("GET error", err)
		}
		c.String(http.StatusNotFound, "No object with that ID")
	} else {
		// Will will need to filter out fields like bto and bcc depending on
		// who is talking to us.
		c.JSON(http.StatusOK, obj.Mapify())
	}
	return
}
